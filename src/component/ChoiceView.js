import React, {Component} from 'react';
import {Dimensions, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import * as Progress from 'react-native-progress'
import Color from '../util/Colors'
import {inject, observer} from "mobx-react";

const width = Dimensions.get("window").width - 75;

@inject('store')
@observer
class ChoiceView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            count: "",
            color: Color.WHITE,
            select: false,
            disable: false
        }
    }

    componentWillReact() {
        const {usersAnswer, resetProgress, questionId} = this.props.store;
        console.log(JSON.stringify(usersAnswer));
        if (Object.keys(usersAnswer).length > 0) {
            //if(questionId === )
            usersAnswer.choices.map((c, i) => {
                if (c.identifier === this.props.choice.identifier) {
                    this.setState({
                        disable: true,
                        progress: c.count / usersAnswer.total,
                        count: c.count
                    })
                }
            })
        } else {
            this.setState({
                disable: false,
                progress: 0,
                count: ""
            })
        }
    }

    componentDidMount() {
        if (this.props.correct) {
            this.setState({
                color: Color.LIME_GREEN
            })
        }
        else {
            this.setState({
                color: Color.LIGHT_GRAY
            })
        }
    }

    render() {
        return (
            <TouchableOpacity
                disabled={this.props.disable || this.state.disable}
                onPress={() => {
                    this.props.onPress(true);
                    this.setState({select: true})
                }}>
                <View
                    style={styles.choiceViewWrapper}
                >
                    <Progress.Bar
                        progress={this.state.progress}
                        width={width}
                        height={60}
                        borderRadius={10}
                        color={this.state.color}
                        borderColor={Color.SEPARATOR_GRAY}
                        style={[styles.choiceViewProgressBarStyle, (this.state.select || this.props.store.eliminate) ? {backgroundColor: Color.WHITE2} : {}]}/>
                    <View style={styles.choiceTextWrapper}>
                        <Text style={styles.choiceViewText}>
                            {this.props.choice.option}
                        </Text>

                        <Text style={styles.choiceUsersAnswered}>
                            {this.state.count}
                        </Text>
                    </View>

                </View>
            </TouchableOpacity>

        );
    }
}

const styles = StyleSheet.create({
    choiceViewWrapper: {
        height: 60,
        width: width,
        marginVertical: 5,
        display: "flex",
    },
    choiceTextWrapper: {
        display: "flex",
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    choiceViewProgressBarStyle: {
        borderRadius: 20,
        color: Color.GRAY,
        alignSelf: "center",
        ...StyleSheet.absoluteFillObject,
    },
    choiceViewText: {
        color: Color.BLACK,
        fontSize: 20,
        marginLeft: 15,
        marginTop: 15,
        fontWeight: 'bold'
    },
    choiceUsersAnswered: {
        color: Color.DARK_GRAY,
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 10,
        alignSelf: 'flex-end',
        marginRight: 10
    },
});

ChoiceView.propTypes = {};
ChoiceView.defaultProps = {};

export default ChoiceView;
