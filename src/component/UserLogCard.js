import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from "../util/Colors";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {inject, observer} from 'mobx-react'
import EmojiAvatar from "./EmojiAvatar";
import TextFit from "./FitTextView";

@inject(['store'])
@observer
class UserLogCard extends Component {

    render() {
        return (
            <View style={styles.userLogCardMainWrapper}>
                <View style={styles.userLogCardBottomWrapper}>
                    <View style={styles.userLogCardTopWrapper}>
                        <View style={styles.userLogCardPrizeWrapper}>
                            <TextFit
                                style={styles.userLogCardPrizeText}
                                height={50}
                                width={75}>
                                {this.props.store.user.won}<Text style={styles.userLogCardPrizeCurrency}>{"ብር"}</Text>
                            </TextFit>

                        </View>
                        <View style={styles.userLogCardLifeWrapper}>
                            <Icon
                                style={styles.userLogCardHeartIcon}
                                name="heart"
                                size={50}
                                color={Colors.MAIN_COLOR}/>
                            <Text style={styles.userLogCardLifeCount}>{this.props.store.user.life}</Text>
                        </View>
                    </View>
                    <View style={styles.userLogCardTextWrapper}>
                        <Text>
                            ሽልማት
                        </Text>
                        <Text>
                            {this.props.store.user.meda_name}
                        </Text>
                        <Text>
                            ሕይወት
                        </Text>
                    </View>
                </View>

                <View style={styles.userLogCardAvatar}>
                    <EmojiAvatar unicode={this.props.store.user.avatar_emoji}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    userLogCardMainWrapper: {
        backgroundColor: Colors.TRANSPARENT_STATUS_BAR,
        alignItems: 'center',
        elevation: 10,
        justifyContent: 'flex-end',
        height: 180,
    },
    userLogCardAbsoluteWrapper: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: 10
    },
    userLogCardTopWrapper: {
        marginHorizontal: 25,
        marginBottom: 5,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    userLogCardLifeWrapper: {
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    userLogCardHeartIcon: {
        ...StyleSheet.absoluteFillObject
    },
    userLogCardLifeCount: {
        color: Colors.WHITE,
        alignSelf: 'center',
        fontSize: 15
    },
    userLogCardPrizeWrapper: {
        flexDirection: 'row'
    },
    userLogCardPrizeText: {
        color: Colors.MAIN_COLOR,
        fontSize: 40,
    },
    userLogCardPrizeCurrency: {
        color: Colors.MAIN_COLOR,
        fontSize: 15,
        marginBottom: 10,
        alignSelf: 'flex-end'
    },
    userLogCardBottomWrapper: {
        height: 120,
        width: "90%",
        backgroundColor: Colors.WHITE,
        paddingBottom: 10,
        justifyContent: 'flex-end',
        borderRadius: 10,
        marginBottom: 20,
    },
    userLogCardTextWrapper: {
        width: "100%",
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-around',
    },
    userLogCardAvatar: {
        position: 'absolute',
        top: 0,
        alignSelf: 'center',
        zIndex: 9999
    },
});

UserLogCard.propTypes = {};
UserLogCard.defaultProps = {};

export default UserLogCard;