import React, {Component} from 'react';
import {StyleSheet, Text, View} from "react-native";
import Color from "../util/Colors";

class ListItem extends Component {
    render() {
        const {index} = this.props;
        const {avatar_emoji, meda_name, won} = this.props.item;
        return (
            <View style={styles.listItemMainWrapper}>
                <View style={styles.listItemLeftWrapper}>
                    <Text style={styles.listItemListCount}>
                        {index + 4}
                    </Text>
                    <Text style={styles.listItemAvatar}>
                        {avatar_emoji}
                    </Text>
                    <Text style={styles.listItemName}>
                        {meda_name}
                    </Text>
                </View>

                <Text style={styles.listItemAmountWon}>
                    {won}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    listItemMainWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: "space-between",
        marginHorizontal: "3%"
    },
    listItemListCount: {
        alignSelf: 'center',
        margin: 10,
        color: Color.WHITE
    },
    listItemAvatar: {
        width: 40,
        height: 40,
        borderRadius: 20,
        marginHorizontal: 10,
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 5,
        backgroundColor: Color.WHITE2,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: Color.LIGHT_GRAY
    },
    listItemName: {
        alignSelf: 'center',
        color: Color.WHITE,
        fontSize: 15
    },
    listItemAmountWon: {
        color: Color.WHITE,
        fontSize: 15,
        alignSelf: 'center',
        backgroundColor: Color.MAIN_COLOR_DARK,
        borderRadius: 10,
        paddingHorizontal: 10,
    },
    listItemLeftWrapper: {
        display: 'flex',
        flexDirection: 'row',
    },
});

ListItem.propTypes = {};
ListItem.defaultProps = {};

export default ListItem;
