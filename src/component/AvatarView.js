import React, {Component} from 'react';
import {Image, StyleSheet, View} from "react-native";
import Color from "../util/Colors";

class AvatarView extends Component {
    render() {
        const {avatarUrl} = this.props;

        return (
            <View style={this.props.avatarStyles}>
                <View style={styles.avatarFirstWrapper}>
                    <View style={styles.avatarSecondWrapper}>
                        <Image
                            style={styles.avatarImage}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    avatarFirstWrapper: {
        borderColor: 'rgba(255, 255, 255, 0.3)',
        borderWidth: 10,
        alignSelf: 'baseline',
        borderRadius: 70,
    },
    avatarSecondWrapper: {
        borderColor: 'rgba(255, 255, 255, 0.6)',
        borderWidth: 7,
        alignSelf: 'baseline',
        borderRadius: 58,
    },
    avatarImage: {
        width: 100,
        height: 100,
        borderWidth: 3,
        borderRadius: 50,
        borderColor: Color.WHITE
    },

});

AvatarView.propTypes = {};
AvatarView.defaultProps = {};

export default AvatarView;
