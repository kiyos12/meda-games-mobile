import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from "react-native";

class AnswerCard extends Component {
    render() {
        return (
            <View>
                <Image
                    source={require('../asset/sibhat.webp')}
                    style={styles.answerCardExplanationSticker}/>
                <View>
                    <Text style={styles.answerCardExplanationText}>
                        {this.props.answer}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    answerCardExplanationSticker: {
        width: 100,
        height: 100,
        zIndex: 9999,
        alignSelf: 'center'
    },
    answerCardExplanationText: {
        margin: 15,
        fontSize: 22,
        fontWeight: 'bold',
        flexDirection: "column"
    },
})

AnswerCard.propTypes = {};
AnswerCard.defaultProps = {};

export default AnswerCard;
