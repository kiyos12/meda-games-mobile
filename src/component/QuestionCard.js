import React, {Component} from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import Color from "../util/Colors";
import ChoiceView from "./ChoiceView";
import Constants from "../util/Constants";
import * as Progress from 'react-native-progress';
import {countDown} from "../util/Time";

let interval;

class QuestionCard extends Component {

    choicePressed = (c) => {
        this.setState({
            disable: true,
            selectedAnswer: c.identifier
        })
    };

    renderChoices = (choices) => {
        return (choices.map((c, i) =>
                <ChoiceView choice={c}
                            questionState={Constants.QUESTION_ANSWERED}
                            onPress={() => this.choicePressed(c)}
                            disable={this.state.disable}
                />
            )
        )
    };

    constructor(props) {
        super(props);
        this.state = {
            progress: 0.0,
            disable: false,
            selectedAnswer: ""
        }
    }

    componentDidMount() {
        interval = countDown(new Date().getTime() + 10000, (isFinished, day, hour, minute, second) => {
            if (isFinished) {
                this.props.timeout(isFinished)
                this.setState({disable: true})
            } else {
                this.setState({
                    progress: (1 - (second + 1) / 10)
                })
            }

        });
    }

    componentWillUnmount() {
        clearInterval(interval);
    }

    render() {
        const {question, answer} = this.props;

        return (
            <View style={styles.questionCardTopMost}>
                <View style={styles.questionCardMainWrapper}>
                    <View style={styles.questionCardTopWrapper}>
                        <View style={styles.questionCardUserCountWrapper}>
                            <Image
                                style={styles.questionCardGroup}
                                source={require('../asset/group.png')}
                            />
                            <Text style={styles.questionCardUserCount}>1,800</Text>
                        </View>

                        {(answer === "") ?
                            <Progress.Circle
                                size={75}
                                progress={this.state.progress}
                                indeterminate={false}
                                thickness={5}
                                formatText={(progress) => {
                                    return 10 - Math.round(progress * 10)
                                }}
                                showsText={true}
                                textStyle={styles.questionCardCountDownProgressText}
                                color={Color.MAIN_COLOR}
                            /> :
                            (this.props.answer === this.state.selectedAnswer) ?
                                <Text style={[styles.questionCardAnswerStatus, {backgroundColor: Color.GREEN}]}>
                                    ተመልሷል
                                </Text> :
                                <Text style={[styles.questionCardAnswerStatus, {backgroundColor: Color.RED}]}>
                                    አልተመለሰም
                                </Text>
                        }
                        <Image
                            style={styles.questionCardLogo}
                            resizeMode="contain"
                            source={require('../asset/logo.png')}
                        />
                    </View>
                    <Text
                        numberOfLines={3}
                        typing={1}
                        style={styles.questionCardQuestionText}>
                        {question.question}
                    </Text>
                    <View style={styles.questionCardChoiceWrapper}>
                        {
                            this.renderChoices(question.choices)
                        }
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    questionCardTopMost: {
        backgroundColor: Color.WHITE,
        borderRadius: 15,
        borderColor: Color.LIGHT_GRAY,
        borderWidth: 2,
        margin: 10,
    },
    questionCardMainWrapper: {
        borderRadius: 15,
        borderColor: Color.LIGHT_GRAY,
        borderWidth: 2,
        padding: 10,
        backgroundColor: Color.WHITE
    },
    questionCardTopWrapper: {
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "row"
    },
    questionCardChoiceWrapper: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
    },
    questionCardQuestionText: {
        textAlign: 'center',
        margin: 15,
        fontSize: 22,
        fontWeight: 'bold',
        flexDirection: "column"
    },
    questionCardCountDownProgressText: {
        color: Color.MAIN_COLOR,
        fontSize: 40,
        fontWeight: 'bold'
    },
    questionCardLogo: {
        width: 75,
        height: 50,
        alignSelf: 'center'
    },
    questionCardGroup: {
        width: 20,
        height: 20,
    },
    questionCardUserCountWrapper: {
        flexDirection: 'row',
        alignSelf: 'center'
    },
    questionCardUserCount: {
        fontSize: 15,
        marginLeft: 5,
        color: Color.DARK_GRAY
    },
    questionCardAnswerStatus: {
        fontSize: 25,
        marginTop: 20,
        fontWeight: 'bold',
        color: Color.WHITE,
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 15
    }

});

QuestionCard.propTypes = {};
QuestionCard.defaultProps = {};

export default QuestionCard;
