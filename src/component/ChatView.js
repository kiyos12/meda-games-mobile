import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, TextInput, TouchableOpacity, View, ViewPagerAndroid, Keyboard} from "react-native";
import {inject, observer} from 'mobx-react'
import Colors from "../util/Colors";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

@inject(['store'])
@observer
class ChatView extends Component {

    renderItem = (item) => {
        return (
            <View style={styles.chatViewSingleMessageWrapper}>
                <Text
                    style={styles.chatViewThumb}>
                    {item.avatar_emoji}
                </Text>
                <Text style={styles.chatViewMedaName}>
                    {item.meda_name+" "}
                    <Text style={styles.chatViewChatText}>
                        {item.text}
                    </Text>
                </Text>

            </View>
        )
    };

    constructor(props) {
        super(props);
        this.state = {
            clicked: false,
            text: ""
        };
        this.store = props.store;
    }

    componentDidMount() {
        this.store.loadMessages(false);
        Keyboard.addListener('keyboardDidHide', ()=> {
            this.setState({clicked: false, text:""});
        });
    }


    /*componentWillReact() {
            alert("CWR "+JSON.stringify(this.store.messages));
        }*/

    render() {
        //alert("Render " + JSON.stringify(this.store.messages));
        return (
            <ViewPagerAndroid
                style={styles.chatViewWrapper}
                initialPage={1}>
                <View style={styles.chatViewPage1} key="1">
                    <Text>Swipe left to Chat</Text>
                </View>
                <View
                    key="2"
                    style={[styles.chatViewWrapper, this.props.chatStyle]}>
                    <FlatList
                        data={this.store.messages.slice()}
                        showsVerticalScrollIndicator={false}
                        renderItem={({item, index}) => {
                            return (
                                this.renderItem(item)
                            )
                        }}
                        ref={ref => this.chatFlatList = ref}
                        onContentSizeChange={() => this.chatFlatList.scrollToEnd({animated: true})}
                        onLayout={() => this.chatFlatList.scrollToEnd({animated: true})}
                    />

                    <View style={styles.chatViewNewMessageWrapper}>
                        {
                            (this.state.clicked) ?
                                <View style={styles.chatViewMessageInputWrapper}>
                                    <TextInput
                                        autoFocus={true}
                                        placeholder='መልእክትዎን ይጻፉ'
                                        style={styles.chatViewMessageInputField}
                                        underlineColorAndroid='transparent'
                                        onChangeText={(text) => this.setState({text})}
                                        onSubmitEditing={() => {
                                            this.store.sendMessage(this.state.text)
                                            this.setState({clicked: false, text:""});
                                        }}
                                    />
                                    <TouchableOpacity style={styles.chatViewMessageSendButton} onPress={()=>{
                                         this.store.sendMessage(this.state.text);
                                         this.setState({clicked: false, text:""});
                                    }}>
                                        <Icon name="send" size={25}/>
                                    </TouchableOpacity>
                                    
                                </View>
                    
                                 :
                                <TouchableOpacity style={styles.chatViewCreateButton}
                                                  onPress={() => this.setState({clicked: true})}>
                                    <Icon
                                        name="message"
                                        size={24}
                                        color={Colors.WHITE}/>
                                </TouchableOpacity>
                        }
                    </View>
                </View>
            </ViewPagerAndroid>

        );
    }
}

const styles = StyleSheet.create({
    chatViewWrapper: {
        height: 150,
        padding: 5,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
    },
    chatViewPage1: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    chatViewSingleMessageWrapper: {
        flex: 1,
        flexDirection: "row",
        marginRight: 10,
    },
    chatViewThumb: {
        width: 25,
        height: 25,
        borderRadius: 12,
        marginRight: 5,
        textAlign: 'center',
        fontSize: 15
    },
    chatViewMedaName: {
        color: Colors.OPACE_WHITE,
        alignSelf: 'center',
        fontSize: 15,
        paddingRight: 20,
    },
    chatViewChatText: {
        color: Colors.WHITE,
        alignSelf: 'center',
        fontSize: 15,
    },
    chatViewCreateButton: {
        alignSelf: 'flex-end',
        borderRadius: 50,
        backgroundColor: Colors.INNER_BORDER,
        padding: 10,
        margin: 10,
    },
    chatViewNewMessageWrapper: {
        width: '100%',
        position: 'absolute',
        bottom: 5,    
        right: 5,
    },
    chatViewMessageInputField:{
        width: "88%",
    },
    chatViewMessageSendButton:{
        alignSelf:'center',
    },
    chatViewMessageInputWrapper: {
        flexDirection: "row",
        justifyContent:'space-between',
        backgroundColor: Colors.WHITE,
        width: "90%",
        fontSize: 16,
        paddingHorizontal: 15,
        marginHorizontal: 20,
        borderRadius: 20,
    },
});

export default ChatView;
