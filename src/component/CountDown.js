import React, {Component} from 'react';
import {StyleSheet, Text, View} from "react-native";
import {countDown} from "../util/Time";
import Colors from "../util/Colors";

class CountDown extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minute: "00",
            second: "00"
        }
    }

    componentWillMount() {
        countDown(new Date(), (isFinished, days, hours, minutes, seconds) => {
            this.setState({
                minute: minutes,
                second: seconds
            })
        })
    }

    render() {
        return (
            <View>
                <Text style={styles.countDownText}>{`${this.state.minute}:${this.state.second}`}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    countDownText: {
        fontSize: 50,
        fontWeight: 'bold',
        alignSelf:"center",
        color: Colors.WHITE
    }
});

CountDown.propTypes = {};
CountDown.defaultProps = {};

export default CountDown;
