import React, {Component} from 'react';
import {StyleSheet, Text, ToastAndroid, Vibration, View} from "react-native";
import {inject, observer} from 'mobx-react'
import ChoiceView from "./ChoiceView";
import Color from "../util/Colors";
import * as Animatable from 'react-native-animatable'
import AnswerCard from "./AnswerCard";
import SFX from "../util/Sound";

let mainTimeout;

@inject('store')
@observer
class QandACard extends Component {

    renderChoices = (id, choices) => {
        return (choices.map((c, i) =>
                <ChoiceView
                    correct={c.identifier === this.props.question.correctAnswer}
                    choice={c}
                    questionId={id}
                    disable={this.state.disable || this.props.store.eliminate}
                    onPress={() => {
                        SFX.play('tap.mp3');
                        this.props.store.pushSelectedAnswer(this.props.store.game._id, id, c.identifier);
                        Vibration.vibrate(50);
                        this.setState({disable: true, selectedAnswer: c.identifier})
                        this.props.selectedAnswer(c.identifier)
                    }}
                />
            )
        )
    };

    resetMainTimeOut = () => {
        //this method is executed whenever a new question is arrived
        if (typeof mainTimeout !== "undefined") {
            clearTimeout(mainTimeout);
            //ToastAndroid.show("ኢንተርኔቶ ደህና አይመስልም...", ToastAndroid.SHORT);
        }
        mainTimeout = setTimeout(() => {
            this.animateQuestionCardRef.setAnimation('bounceOutRight');
            this.animateQuestionCardRef.startAnimation(2000);
            if (this.state.selectedAnswer !== this.props.question.correctAnswer) {
                this.props.store.lostQuestion();
            } else {
                ++this.props.store.answeredSoFar;
            }
            this.setState({selectedAnswer: ""})
        }, 26500)
    };

    showQuestion = () => {
        this.resetQuestionTimeOut();
    };

    resetQuestionTimeOut = () => {
        //this method executed when the question is arrived for 10 sec
        setTimeout(() => {
            this.props.store.resetUserAnswer();
            let showAnswer = SFX.play("show_answer.mp3");
            SFX.volume(showAnswer, 0.1);
            this.setState({
                explanation: true,
                disable: false,
            });
        }, 17000)
    };

    constructor(props) {
        super(props);
        this.state = {
            explanation: false,
            disable: false,
            selectedAnswer: "",
            resetProgress: false
        }
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (nextProps.question._id !== this.props.question._id) {
            this.setState({explanation: false});
            this.animateQuestionCardRef.stopAnimation();
            this.animateQuestionCardRef.setAnimation('bounceInLeft');
            this.animateQuestionCardRef.startAnimation(2000);
            this.resetMainTimeOut();
            this.showQuestion();
        }
    }

    componentDidMount() {
        this.props.store.resetUserAnswer();
        this.resetMainTimeOut();
        this.showQuestion();
    }

    render() {
        const {question} = this.props;
        return (
            <Animatable.View
                ref={ci => this.animateQuestionCardRef = ci}
                animation="bounceInLeft"
                duration={2000}
                style={styles.qAndAMainWrapper}>
                {
                    (!this.state.explanation) ?
                        <Animatable.View>
                            <Text style={styles.qAndAQuestionText}>
                                {question.question}
                            </Text>
                            <View>{this.renderChoices(question._id, question.choices)}</View>
                        </Animatable.View>
                        :
                        < Animatable.View
                            duration={2000}
                            animation="fadeIn"
                            style={styles.qAndAExplanationWrapper}>
                            <AnswerCard answer={question.explanation}/>
                        </Animatable.View>
                }
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    qAndAMainWrapper: {
        backgroundColor: Color.WHITE,
        borderRadius: 15,
        paddingHorizontal: 10,
        margin: 10,
        elevation: 2
    },
    qAndAChoiceWrapper: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around",
    },
    qAndAQuestionText: {
        textAlign: 'center',
        margin: 15,
        fontSize: 22,
        fontWeight: 'bold',
        flexDirection: "column"
    },
    qAndAExplanationWrapper: {
        marginVertical: 15,
    },
});

QandACard.propTypes = {};
QandACard.defaultProps = {};

export default QandACard;
