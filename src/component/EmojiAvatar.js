import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Color from "../util/Colors";
import {inject, observer} from 'mobx-react'

@inject(["store"])
@observer
class EmojiAvatar extends Component {
    render() {
        const {unicode} = this.props;
        return (
            <View>
                <View style={styles.emojiAvatarFirstWrapper}>
                    <View style={styles.emojiAvatarSecondWrapper}>
                        <Text style={styles.emojiAvatar}>
                            {unicode}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    emojiAvatarFirstWrapper: {
        borderColor: 'rgba(255, 255, 255, 0.3)',
        borderWidth: 10,
        alignSelf: 'baseline',
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 70,
    },
    emojiAvatarSecondWrapper: {
        borderColor: 'rgba(255, 255, 255, 0.6)',
        borderWidth: 7,
        flexDirection: 'row',
        alignSelf: 'baseline',
        borderRadius: 58,
    },
    emojiAvatar: {
        width: 100,
        height: 100,
        borderWidth: 3,
        fontSize: 60,
        borderRadius: 50,
        alignSelf: 'center',
        textAlign: 'center',
        borderColor: Color.WHITE,
        backgroundColor: Color.WHITE2
    },
});

EmojiAvatar.propTypes = {};
EmojiAvatar.defaultProps = {};

export default EmojiAvatar;
