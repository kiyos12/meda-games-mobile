import React, {Component} from 'react';
import LinearGradient from "react-native-linear-gradient";
import Color from "../util/Colors";

class ChatGradient extends Component {
    render() {
        return (
            <LinearGradient
                colors={[Color.TRANSPARENT_STATUS_BAR, Color.GRADIENT_END]}>
                {this.props.children}
            </LinearGradient>
        );
    }
}

ChatGradient.propTypes = {};
ChatGradient.defaultProps = {};

export default ChatGradient;
