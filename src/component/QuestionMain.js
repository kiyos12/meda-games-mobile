import React, {Component} from 'react';
import {Dimensions, Image, StyleSheet, Text, View} from "react-native";
import Color from "../util/Colors";
import * as Progress from 'react-native-progress';
import QandACard from "./QandACard";
import {inject, observer} from 'mobx-react';
import {countDown} from "../util/Time";
import SFX from "../util/Sound";
import * as Animatable from 'react-native-animatable';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

@inject(['store'])
@observer
class QuestionMain extends Component {

    startCountDown = (server_time) => {
        SFX.play('question.mp3');
        console.log("CountDownTime " + server_time);
        countDown(new Date().getTime() + 10000, (isFinished, day, hour, minute, second) => {
            if (isFinished) {
                this.setState({
                    progressBarColor: Color.LIGHT_GRAY
                })
            } else {
                this.setState({
                    progress: (1 - (second + 1) / 10),
                    progressBarColor: Color.MAIN_COLOR
                })
            }
        });
    };

    resetCountDown = () => {
        this.setState({
            progress: 0.0,
            timeUp: false,
            selectedAnswer: ""
        })
    };

    startAnswerDetector = () => {
        setTimeout(() => {
            this.setState({timeUp: true});
            if (this.state.selectedAnswer === this.props.question.correctAnswer) {
                SFX.play('correct.m4a');
            }
            else {
                SFX.play('soft_fail.mp3');
            }
        }, 12000);
    };

    constructor(props) {
        super(props);
        this.state = {
            progress: 0.0,
            progressBarColor: Color.MAIN_COLOR,
            selectedAnswer: "",
            timeUp: false
        }
    }

    componentWillMount() {
        this.startCountDown(this.props.question.server_time);
        this.startAnswerDetector();
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        if (nextProps.question._id !== this.props.question._id) {
            this.props.store.resetUserAnswer();
            this.resetCountDown();
            this.startCountDown(this.props.question.server_time);
            this.startAnswerDetector();
        }
    }

    render() {
        return (
            <View>
                <View style={styles.questionMainMainWrapper}>
                    <View style={styles.questionMainTopWrapper}>
                        <View style={styles.questionMainUserCountWrapper}>
                            <Image
                                style={styles.questionMainGroup}
                                source={require('../asset/group.png')}
                            />
                            <Text style={styles.questionMainUserCount}>{this.props.store.usersCount.count}</Text>
                        </View>
                        {
                            (!this.state.timeUp || this.props.store.eliminate) ?
                                <Progress.Circle
                                    style={styles.questionMainProgress}
                                    size={75}
                                    progress={this.state.progress}
                                    indeterminate={false}
                                    thickness={5}
                                    formatText={(progress) => {
                                        return 10 - Math.round(progress * 10)
                                    }}
                                    showsText={true}
                                    textStyle={
                                        [styles.questionMainCountDownProgressText,
                                            {color: this.state.progressBarColor}
                                        ]}
                                    color={this.state.progressBarColor}
                                /> :
                                (this.props.question.correctAnswer === this.state.selectedAnswer) ?
                                    <Animatable.Text
                                        duration={1000}
                                        animation="bounceIn"
                                        style={[styles.questionMainAnswerStatus, {backgroundColor: Color.GREEN}]}>
                                        ተመልሷል
                                    </Animatable.Text> :
                                    <Animatable.Text
                                        duration={1000}
                                        animation="bounceIn"
                                        style={[styles.questionMainAnswerStatus, {backgroundColor: Color.RED}]}>
                                        አልተመለሰም
                                    </Animatable.Text>
                        }
                        <View style={styles.questionMainLifeWrapper}>
                            {/* <Text style={styles.questionMainLifeText}>
                                ሕይወት
                            </Text>*/}
                            <Icon
                                name="heart"
                                size={30}
                                style={styles.questionMainHeartIcon}
                                color={Color.MAIN_COLOR}/>
                            <Text style={styles.questionMainLifeCount}>{this.props.store.user.life}</Text>

                        </View>
                    </View>
                    <QandACard
                        selectedAnswer={(identifier) => {
                            this.setState({
                                selectedAnswer: identifier
                            })
                        }}
                        question={this.props.question}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    questionMainMainWrapper: {
        margin: 10,
        borderRadius: 15,
        borderColor: Color.LIGHT_GRAY,
        borderWidth: 2,
        padding: 3,
        height: "82%",
        backgroundColor: Color.WHITE,
        zIndex: 9999,
    },
    questionMainProgress: {
        position: 'absolute',
        left: (Dimensions.get('window').width / 2) - 55,
    },
    questionMainTopWrapper: {
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "row",
        height: 75
    },
    questionMainCountDownProgressText: {
        color: Color.MAIN_COLOR,
        fontSize: 40,
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    questionMainLogo: {
        width: 50,
        height: 50,
        alignSelf: 'center',
        backgroundColor: Color.GREEN
    },
    questionMainGroup: {
        width: 20,
        height: 20,
    },
    questionMainUserCountWrapper: {
        flexDirection: 'row',
        margin: 10
    },
    questionMainUserCount: {
        fontSize: 16,
        marginLeft: 5,
        color: Color.DARK_GRAY,
        alignSelf: 'flex-start'
    },
    questionMainLifeText: {
        fontSize: 19,
        color: Color.DARK_GRAY,
    },
    questionMainAnswerStatus: {
        position: 'absolute',
        left: (Dimensions.get('window').width / 2) - 90,
        fontSize: 25,
        fontWeight: 'bold',
        alignSelf: 'center',
        color: Color.WHITE,
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 15
    },
    questionMainLifeWrapper: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10
    },
    questionMainHeartIcon: {
        ...StyleSheet.absoluteFillObject
    },
    questionMainLifeCount: {
        color: Color.WHITE,
        alignSelf: 'center',
        fontSize: 11
    },
});

QuestionMain.propTypes = {};
QuestionMain.defaultProps = {};

export default QuestionMain;
