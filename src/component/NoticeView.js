import React, {Component} from 'react';
import {StyleSheet, View} from "react-native";
import Colors from "../util/Colors";
import * as Animatable from 'react-native-animatable';
import {inject, observer} from "mobx-react";

@inject(["store"])
@observer
class NoticeView extends Component {
    loopSound;

    constructor(props) {
        super(props);
        this.state = {
            visibleNotice: "ጨዋታው ሊጀመር ነው"
        }
    }

    componentDidMount() {
        //this.loopSound = SFX.play('loop.aac', true);
        let i = 1;
        setInterval(() => {
            if (this.animateNoticeView) {
                this.animateNoticeView.setAnimation('zoomOut');
                this.animateNoticeView.startAnimation(1000);
                this.animateNoticeView.setAnimation('zoomIn');
                this.animateNoticeView.startAnimation(1000);
                this.setState({
                    visibleNotice: this.props.store.news[i]
                });
                ++i;
                if (i === this.props.store.news.length - 1) {
                    i = 0;
                }
            }
        }, 3000)
    }

    componentWillUnmount() {
        //this.loopSound.stop()
    }

    render() {
        return (
            <View>
                {this.props.visible ?
                    <Animatable.Text
                        ref={r => this.animateNoticeView = r}
                        duration={1000}
                        animation='fadeIn'
                        style={styles.noticeViewText}>
                        {this.state.visibleNotice}
                    </Animatable.Text>
                    : <View/>}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    noticeViewText: {
        margin: 10,
        paddingHorizontal: 5,
        alignSelf: 'center',
        fontSize: 30,
        color: Colors.WHITE,
        fontWeight: 'bold',
        textAlign: 'center',
        zIndex: 9999
    }
})

NoticeView.propTypes = {};
NoticeView.defaultProps = {};

export default NoticeView;