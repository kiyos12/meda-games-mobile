import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import Colors from "../util/Colors";

class Button extends Component {

    render() {
        const {text, onPress} = this.props;

        return (
            <TouchableOpacity onPress={() => onPress()} style={styles.buttonTouchableOpacity}>
                <Text style={styles.buttonWrapper}>
                    {text}
                </Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    buttonTouchableOpacity: {
        backgroundColor: Colors.TRANSPARENT_STATUS_BAR
    },
    buttonWrapper: {
        backgroundColor: Colors.WHITE,
        color: Colors.BLACK,
        textAlign: 'center',
        paddingHorizontal: 20,
        width: 100,
        paddingVertical: 12,
        elevation: 5,
        borderRadius: 10
    }
});

Button.propTypes = {};
Button.defaultProps = {};

export default Button;
