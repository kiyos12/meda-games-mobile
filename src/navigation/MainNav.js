import {StackNavigator} from "react-navigation";
import LeaderBoard from "../screen/LeaderBoard";
import QuestionScreen from "../screen/QuestionScreen";
import WinnerScreen from "../screen/WinnerScreen";
import ExtraLife from "../screen/ExtraLife";
import DayScreen from "../screen/DayScreen";
import IntroScreen from "../screen/IntroScreen";
import FAQ from "../screen/FAQ";
import Color from "../util/Colors";

export default StackNavigator({
    DayScreen: {
        screen: DayScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    IntroScreen: {
        screen: IntroScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    WinnerScreen: {
        screen: WinnerScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    LeaderBoard: {
        screen: LeaderBoard,
        navigationOptions: ({navigation}) => ({
            title: "የጨዋታ መሪዎች",
            headerTintColor: Color.WHITE,
            titleStyle: {
                color: Color.WHITE,
                fontWeight: '300',
            },
            headerStyle: {
                display: "flex",
                backgroundColor: Color.GRADIENT_START,
                textAlign: 'center',
            },

        })
    },
    QuestionScreen: {
        screen: QuestionScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    ExtraLife: {
        screen: ExtraLife,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    FAQ: {
        screen: FAQ,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
});