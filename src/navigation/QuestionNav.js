import {StackNavigator} from "react-navigation";
import LeaderBoard from "../screen/LeaderBoard";
import QuestionScreen from "../screen/QuestionScreen";
import WinnerScreen from "../screen/WinnerScreen";
import DayScreen from "../screen/DayScreen";

export default StackNavigator({

    DayScreen: {
        screen: DayScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    QuestionScreen: {
        screen: QuestionScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    LeaderBoard: {
        screen: LeaderBoard,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    WinnerScreen: {
        screen: WinnerScreen,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
});