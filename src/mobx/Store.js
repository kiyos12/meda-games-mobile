import {Alert, AsyncStorage, ToastAndroid} from 'react-native';
import {action, observable} from 'mobx';
import {autobind} from 'core-decorators';
import io from 'socket.io-client/dist/socket.io';
import feathers from 'feathers/client'
import hooks from 'feathers-hooks';
import customMethod from 'feathers-custom-methods/client';
import socketio from 'feathers-socketio/client';
import authentication from 'feathers-authentication-client';
import MedaModal from "../util/MedaModal";
import SFX from "../util/Sound";
import {Notices} from "../util/Constants";

//const API_URL = 'http://192.168.43.238:3030';
//const API_URL = 'http://192.168.0.32:3030';
const API_URL = 'http://178.62.39.146:3030';

@autobind
export default class Store {

    loggedIn = false;
    disconnectIntentional = false;
    @observable isLifeSubtracted = false;
    @observable isAuthenticated = false;
    @observable isConnecting = false;
    @observable user = {};
    @observable users = [];
    @observable usersCount = {'type': 'usersonline', 'count': 0};
    @observable usersAnswer = {};
    @observable messages = [];
    @observable questions = {};
    @observable news = [];
    @observable timeLapse = 0;
    @observable game = {};
    @observable hasMoreMessages = false;
    @observable skip = 0;
    @observable selectedChoices = [];
    @observable eliminate = false;
    @observable lifeUsed = false;
    @observable gameFinished = false;
    @observable started = false;
    @observable answeredSoFar = 0;
    @observable resetProgress = false;
    @observable startIntro = false;

    constructor(main) {
        const options = {transports: ['websocket'], pingTimeout: 5000, pingInterval: 5000};
        const socket = io(API_URL, options);

        this.app = feathers()
            .configure(socketio(socket))
            .configure(hooks())
            .configure(authentication({
                storage: AsyncStorage
            }))
            .configure(customMethod({
                methods: {
                    game: ['answer'],
                    users: ['findCreate']
                }
            }));

        this.connect();

        this.app.service('messages').on('created', createdMessage => {
            //alert("Message Created ", JSON.stringify(createdMessage));
            if (createdMessage.user_id !== this.user._id)
                this.messages.push(createdMessage);
        });

        this.app.service('messages').on('removed', removedMessage => {
            this.deleteMessage(removedMessage);
        });

        this.app.service('question').on('updated', question => {
            this.questions = question;
        });

        this.app.service('users').on('usersonline', count => {
            this.usersCount = count;
        });

        this.app.service('game').on('startIntro', response => {
            this.timeLapse = (new Date(response.start_time) - new Date().getTime()) / 1000;
            this.game = response;
        });

        this.app.service('game').on('gamefinished', response => {
            //this.resetQuestions();
            this.gameFinished = true;
            if (!this.eliminate) {
                SFX.play('victory.mp3');
                MedaModal.showWinnerModal(this.user.meda_profile_pic, response.each);
            } else {
                ToastAndroid.show("ጨዋታው ተጠናቋል ", ToastAndroid.SHORT);
            }
        });

        this.app.service('game').on('usersAnswer', response => {
            console.log("From store " + JSON.stringify(response));
            this.usersAnswer = response;
        });

        if (this.app.get('accessToken')) {
            this.isAuthenticated = this.app.get('accessToken') !== null;
        }
    }

    @action
    getGameNoticeNews(_id) {
        return this.app.service('news').find({
            query: {
                game: _id
            }
        }).then(res => {
            if (res.data.length > 0)
                this.news = res.data[0].news;
            else
                this.news = Notices
        }).catch(() => {
            this.news = Notices
        })
    }

    @action
    getScheduledGame() {
        const query = {query: {scheduled: "true"}};
        return this.app.service('game').find(query).then(response => {
            this.getGameNoticeNews(response.data[0]._id)
            this.timeLapse = (new Date(response.data[0].start_time) - new Date().getTime()) / 1000;
            this.game = response.data;
        }).catch(error => {
            console.log(error);
        });
    }

    @action
    resetUserAnswer() {
        this.usersAnswer = {}
    }

    @action
    resetQuestions() {
        this.questions = {}
    }

    @action
    getStartedGame() {
        const query = {query: {started: "true"}};
        return this.app.service('game').find(query).then(response => {
            if (response.data.length > 0) {
                MedaModal.showLateModal();
                this.eliminate = true;
            }
        }).catch(error => {
            console.log(error);
        });
    }

    @action
    disconnect() {
        this.disconnectIntentional = true;
        this.app.io.disconnect();
    }

    @action
    pushSelectedAnswer(game_id, question_id, answer) {
        const user = this.user.phone_number;
        return this.app.service('game').answer({user, game_id, question_id, answer});
    }

    @action
    updateResetProgress(status) {
        this.resetProgress = status;
        console.log("Progress " + this.resetProgress);
    }

    @action
    addToSelectedChoice(question, choice) {
        this.selectedChoices.concat({"question": question, "selected": choice})
    }

    @action
    updateHighscore(score) {
        this.app.service('users').update(this.user._id, {
            $set: {
                highscore: score
            }
        })
    }

    @action
    updateLife(user, life) {
        this.app.service('users').update(user, {
            $set: {
                life: life
            }
        }).then(response => {
            console.log("Update " + JSON.stringify(response))
        }).catch(error => console.log("Update Error " + error))
    }

    @action
    updateReferrerLife(phone) {
        if (phone === this.user.phone_number) {
            ToastAndroid.show("አረ ይደብራል ", ToastAndroid.SHORT);
            return;
        }
        const query = {query: {phone_number: phone}};
        this.app.service('users').find(query).then((response) => {
            this.updateLife(response.data[0]._id, response.data[0].life + 1)
        })
            .then(res => ToastAndroid.show("ሁሌም ቢሆን መረዳዳት ጥሩ ነገር ነው 👍🏼", ToastAndroid.SHORT))
            .catch(err => ToastAndroid.show("ሲመስለኝ ያስገቡት ቁጥር እኛ ጋር አልተመዘገበም", ToastAndroid.SHORT));
    }

    @action
    subtractLocalLife() {
        this.user.life = this.user.life - 1
    }

    @action
    lostQuestion() {/*
        if (!this.isLifeSubtracted) {
            this.isLifeSubtracted = true;
            this.subtractLocalLife()
        }*/
        if (this.user.life > 1 && !this.lifeUsed && !this.eliminate) {
            MedaModal.showSavedModal();
            SFX.play("saved.mp3");
            this.lifeUsed = true;
            this.pushSelectedAnswer(this.game._id, this.questions._id, this.questions.correctAnswer);
            this.updateLife(this.user._id, this.user.life - 1);
        }

        else {
            if (this.answeredSoFar > this.user.highscore) {
                SFX.play('highscore.mp3');
                MedaModal.showModal(this.answeredSoFar);
                this.updateHighscore(this.answeredSoFar);
            } else {
                if (!this.eliminate) {
                    SFX.play('eliminated.mp3');
                    MedaModal.showEliminateModal();
                }
            }
            this.eliminate = true;
        }
    }

    connect() {
        this.isConnecting = true;

        this.app.io.on('connect', () => {
            this.getScheduledGame();
            this.authenticate().then(() => {
                    this.loggedIn = true;
                    this.isConnecting = false;
                    ToastAndroid.show('እንኳን ደህና መጡ \ud83d\ude01', ToastAndroid.SHORT);
                    MedaModal.toggleInternetDisconnectedModal(false);
                }
            ).catch(error => {
                this.createAccount();
            });
        });

        this.app.io.on('disconnect', () => {
            ToastAndroid.show('ግንኙነቱ ተቋርጧል \ud83d\ude30', ToastAndroid.SHORT);
            if (!this.disconnectIntentional) {
                MedaModal.toggleInternetDisconnectedModal(true);
                this.isConnecting = true;
            }
        });
    }

    createAccount() {
        // Todo here replace dummy data with actual data coming from meda
        //alert(user);
        const userData = {
            "meda_id": "134758769",
            "meda_name": "Biruk",
            "phone_number": "251911343797",
            "meda_profile_pic": "http://www.nretnil.com/avatar/LawrenceEzekielAmos.png",
            "email": "251911343797@meda.im",
            "password": "251911343797",
            "won": 0,
            "highscore": 0,
            "life": 0
        };

        if (Object.keys(userData).length === 0) {
            this.createAccount();
        }

        else {
            return this.app.service('users').create(userData).then((result) => {
                return this.authenticate(Object.assign(userData, {strategy: 'local'}))
                // .then(res => alert(JSON.stringify(res)))
                // .catch(err => alert(JSON.stringify(err)))
            }).catch(e => console.log("error " + JSON.stringify(e)));
        }
        // return Util.getUser((user) => {
        //     alert(user);
        //     const userData = JSON.parse(user);
        //     if (Object.keys(userData).length === 0) {
        //         this.createAccount();
        //     } else {
        //         return this.app.service('users').create(userData).then((result) => {
        //             alert(JSON.stringify(result));
        //             return this.authenticate(Object.assign(userData, {strategy: 'local'}))
        //                 .then(res => console.log(JSON.stringify(res)))
        //                 .catch(err => console.log(JSON.stringify(err)))
        //         }).catch(e => console.log("error " + JSON.stringify(e)));
        //     }
        // })
    }

    login(payload) {
        return this.authenticate();
    };

    @action
    getUsers() {
        this.app.service("users").find({
                query: {
                    $sort: {
                        won: -1
                    }
                }
            }
        ).then(response => {
            this.users = response.data;
        });
    }

    @action
    authenticate(options) {
        options = options ? options : undefined;
        return this._authenticate(options).then(user => {
            this.user = user;
            this.isAuthenticated = true;
            return Promise.resolve(user);
        });
    }

    @action
    _authenticate(payload) {
        return this.app.authenticate(payload)
            .then(response => {
                return this.app.passport.verifyJWT(response.accessToken);
            })
            .then(payload => {
                return this.app.service('users').get(payload.userId);
            }).catch(e => Promise.reject(e));
    }

    promptForLogout() {
        Alert.alert('Sign Out', 'Are you sure you want to sign out?',
            [
                {
                    text: 'Cancel', onPress: () => {
                }, style: 'cancel'
                },
                {text: 'Yes', onPress: this.logout, style: 'destructive'},
            ]
        );
    }

    logout() {
        this.app.logout();
        this.skip = 0;
        this.messages = [];
        this.user = null;
        this.isAuthenticated = false;
    }

    loadMessages(loadNextPage) {
        let $skip = this.skip;

        return this.app.service('messages').find().then(response => {
            const messages = [];
            const skip = response.skip + response.limit;

            for (let message of response.data) {
                messages.push(message);
            }
            if (!loadNextPage) {
                this.messages = messages;
            } else {
                this.messages = this.messages.concat(messages);
            }
            this.skip = skip;
            this.hasMoreMessages = response.skip + response.limit < response.total;

        }).catch(error => {
            console.log(error);
        });
    }

    deleteMessage(messageToRemove) {
        let messages = this.messages;
        let idToRemove = messageToRemove.id ? messageToRemove.id : messageToRemove._id;

        messages = messages.filter(function (message) {
            return message.id !== idToRemove;
        });
        this.messages = messages;
    }

    sendMessage(text) {
        if (text.length !== 0) {
            let messageContent = {
                user_id: this.user._id,
                meda_name: this.user.meda_name,
                avatar_emoji: this.user.avatar_emoji,
                text: text
            };
            this.messages.push(messageContent);
            this.app.service('messages').create(messageContent)
        }
    }
}