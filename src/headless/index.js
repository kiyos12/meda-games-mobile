import io from 'socket.io-client/dist/socket.io';
import feathers from 'feathers/client'
import hooks from 'feathers-hooks';
import socketio from 'feathers-socketio/client'

const API_URL = 'http://178.62.39.146:3030';

const options = {transports: ['websocket'], pingTimeout: 3000, pingInterval: 5000};
const socket = io(API_URL, options);

this.app = feathers()
    .configure(socketio(socket))
    .configure(hooks());

export default async (bundle) => {
    console.log("GameService " + JSON.stringify(bundle));

    this.app.service('question').on('updated', question => {
        console.log("GameService " + JSON.stringify(question));
    });
};