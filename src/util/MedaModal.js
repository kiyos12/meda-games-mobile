import {NativeModules} from 'react-native';

let shown = false;

export default {
    showModal: (answers) => {
        if (!shown) {
            shown = true;
            NativeModules.medamodal.showModal(answers)
        }
    },

    showSavedModal: () => {
        NativeModules.medamodal.showSavedModal()
    },

    showLateModal: () => {
        NativeModules.medamodal.showLateModal()
    },

    showWinnerModal: (avatarUrl, each) => {
        if (!shown) {
            shown = true;
            NativeModules.medamodal.showWinnerModal(avatarUrl, each)
        }
    },

    showEliminateModal: () => {
        NativeModules.medamodal.showEliminateModal()
    },

    toggleInternetDisconnectedModal: (isShow) => {
        NativeModules.medamodal.toggleInternetDisconnectedModal(isShow)
    }
}