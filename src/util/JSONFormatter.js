export const getUserJSON = (meda_id, email, meda_name, phone_number, password, meda_profile_pic) => {
    return {
        "meda_id": meda_id,
        "meda_name": meda_name,
        "phone_number": phone_number,
        "meda_profile_pic": meda_profile_pic,
        "email": email,
        "password": phone_number,
        "questions": [],
        "won": 0,
        "highscore": 0,
        "life": 1,
    }
};

export const getAnsweredQuestion = (game_id, question_id, selected) => {
    return {
        "game_id": game_id,
        "question_id": question_id,
        "selected": selected,
    }
};