import {requireNativeComponent, ViewPropTypes} from 'react-native'
import PropTypes from 'prop-types'

const iface = {
    name: 'ParticleView',
    propTypes: {
        start: PropTypes.bool,
        ...ViewPropTypes, //include the default view properties
    },
};

export default requireNativeComponent('ParticleView', iface)