import {requireNativeComponent, ViewPropTypes} from 'react-native'
import PropTypes from 'prop-types'

const iface = {
    name: 'ConfettiView',
    propTypes: {
        start: PropTypes.bool,
        ...ViewPropTypes, //include the default view properties
    },
};

export default requireNativeComponent('ConfettiView', iface)