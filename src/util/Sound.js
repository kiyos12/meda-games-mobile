import Sound from 'react-native-sound'

Sound.setCategory('Playback');

let soundObj = null;

export default {
    play: (path, loop, to, onFinished) => {
        soundObj = new Sound(path, Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                console.log('failed to load the sound', error);
                return;
            }

            if (typeof to !== 'undefined')
                soundObj.setCurrentTime(to);

            if (loop)
                soundObj.setNumberOfLoops(-1);

            soundObj.play((success) => {
                if (typeof onFinished !== 'undefined')
                    onFinished(success);
                    soundObj.reset();
            });
            // loaded successfully
            console.log('duration in seconds: ' + soundObj.getDuration() + 'number of channels: ' + soundObj.getNumberOfChannels());
        });
        return soundObj;
    },

    stop: (soundObj) => {
        if(typeof soundObj !== "undefined"){
            soundObj.stop();
            soundObj.release();
        }
    },

    jump: (to) => {
        soundObj.setCurrentTime(to);
    },

    volume: (soundObj, volume) => {
        soundObj.setVolume(volume);
    }

}