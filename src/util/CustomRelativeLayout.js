import {requireNativeComponent, ViewPropTypes, View} from 'react-native'
import PropTypes from 'prop-types'

const iface = {
    name: 'CustomRelativeLayout',
    propTypes: {
        ...View.propTypes,
        ...ViewPropTypes, //include the default view properties
    },
};

export default requireNativeComponent('CustomRelativeLayout', iface)