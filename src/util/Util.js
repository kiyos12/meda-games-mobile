import {NativeModules} from 'react-native'
import {NavigationActions} from "react-navigation";

export default {
    addEvent: (details) => NativeModules.Util.addEvent(details),
    getUser: (callback) => NativeModules.Util.getUser(callback),
    showNotification: (message) => NativeModules.Util.showNotification(message),
    handleResetNavigation: (route, navigation) => {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: route})
            ]
        });
        navigation.dispatch(resetAction)
    }
}