export default {
    QUESTION_ANSWERED: 'question_answered',
    QUESTION_NOT_ANSWERED: 'question_not_answered',
    QUESTION_DEFAULT: 'question_default'
};

export const Notices = [
    "ጨዋታው ሊጀመር ነው ",
    "ኢንተርኔቶ ጥሩ መሆኑን ያረጋግጡ ",
    "በሰዓት መግባትዎን ያረጋግጡ",
    "ከዘገዩ ተመልካች ብቻ ነዎት",
    "በኃላፊነት ቻት ያርጉ",
    "ተጨማሪ ህይወት ለማሸነፍ ያግዞታል",
    "ጨዋታው ላይ ምንም Button አይጫኑ",
    "Button መጫን ከጨዋታ ውጪ ያደርጋል",
    "መልሱን በዐሥር ሰከንዶች ይምረጡ",
    "ኢንተርኔት ከተቋረጠ ከጨዋታ ውጪ ይሆናሉ",
    "አንድ ጥያቄ ከተሳሳቱ ከጨዋታ ውጪ ይሆናሉ",
    "ያሸነፉትን ገንዘብ ከዳሽን ባንክ ይውሰዱ",
    "ከፈለጉ እዚሁ ሜዳ ላይ credit መግዛት",
    "ክጓደኛዎ ጋር  ተጋግዛችሁ መልሱ \ud83d\ude01",
];

//export const formatter = new Intl.NumberFormat('en-US', {style: 'decimal',minimumFractionDigits: 0,})