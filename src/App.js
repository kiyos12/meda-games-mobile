import React, {Component} from 'react';
import {NetInfo, ProgressBarAndroid, StatusBar, StyleSheet, Text, ToastAndroid, View} from 'react-native';
import Store from "./mobx/Store";
import {observer, Provider} from "mobx-react";
import MainNav from "./navigation/MainNav";
import Colors from "./util/Colors";
import ChatView from "./component/ChatView";
import StatusBarAlert from "react-native-statusbar-alert";

type Props = {};
@observer
export default class App extends Component<Props> {
    constructor(props) {
        super(props);
        this.store = new Store();
        this.store.getStartedGame();
        this.state = {
            showChat: false
        }
    }

    componentDidMount() {
        NetInfo.addEventListener('connectionChange', (connectionInfo) => {
            ToastAndroid.show("Connection type " + connectionInfo.effectiveType, ToastAndroid.SHORT);
        })
    }

    componentWillUnmount() {
        this.store.disconnect();
    }


    render() {
        return (
            <Provider store={this.store}>
                <View style={styles.container}>
                    <StatusBar
                        backgroundColor={Colors.MAIN_COLOR_DARK}
                        barStyle="light-content"
                    />
                    <StatusBarAlert
                        visible={this.store.isConnecting}
                        backgroundColor={Colors.MAIN_COLOR}
                        color="white">
                        <View style={styles.connectionStateWrapper}>
                            <Text style={styles.connectionStateText}>በመገናኘት ላይ</Text>
                            <ProgressBarAndroid color={Colors.WHITE} style={styles.connectionStateLoadingAnimation}/>
                        </View>

                    </StatusBarAlert>
                    <MainNav
                        style={styles.mainNav}
                        onNavigationStateChange={(prevState, currentState) => {
                            /*{"key":"StackRouterRoot","isTransitioning":false,"index":0,"routes":[{"routeName":"DayScreen","key":"id-1526379930310-0"}]}*/
                            let routeName = currentState.routes[currentState.index].routeName;
                            if (routeName === "IntroScreen" || routeName === "QuestionScreen") {
                                this.setState({showChat: true})
                            }
                            else {
                                this.setState({showChat: false})
                            }
                        }}/>
                    {(this.state.showChat) && <ChatView/>}

                </View>

            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    connectionStateWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    connectionStateText: {
        color: Colors.WHITE,
        fontWeight: 'normal',
        marginRight: 5
    },
    connectionStateLoadingAnimation: {
        width: 15,
        height: 15
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    mainNav: {
        width: "100%",
        height: "100%"
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    banner: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        padding: 5,
        backgroundColor: '#E98B50',
        opacity: 0.8
    },
    bannerText: {
        color: 'white',
        fontWeight: '400',
        fontSize: 13,
        textAlign: 'center'
    },
});
