import io from 'socket.io-client';
import feathers from 'feathers';
import socketio from 'feathers-socketio';
import authentication from 'feathers-authentication-client';
import {AsyncStorage} from 'react-native';

export const socket = io('http://192.168.0.32:3030');

const client = feathers();

client.configure(socketio(socket));

client.configure(authentication({
    storage: AsyncStorage
}));

export default client