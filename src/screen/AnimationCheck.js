import React, {Component} from 'react';
import Main from "./main";
import * as Animatable from 'react-native-animatable';
import {Text, TouchableOpacity} from "react-native";

const zoomOut = {
    0: {
        opacity: 1,
        scale: 1,
    },
    0.5: {
        opacity: 1,
        scale: 0.3,
    },
    1: {
        opacity: 0,
        scale: 0,
    },
};

class AnimationCheck extends Component {
    handleTextRef = ref => this.text = ref;

    render() {
        return (
            <Main>
                <Animatable.View
                    animation="shake"
                    direction="alternate">
                    <Text>
                        Up and down you go
                    </Text>
                    <Text>
                        Up and down you go
                    </Text>

                </Animatable.View>
                <TouchableOpacity onPress={() => this.text.transitionTo({opacity: 0.5})}>
                    <Animatable.Text ref={this.handleTextRef}>Fade me!</Animatable.Text>
                </TouchableOpacity>
            </Main>
        );
    }
}

AnimationCheck.propTypes = {};
AnimationCheck.defaultProps = {};

export default AnimationCheck;
