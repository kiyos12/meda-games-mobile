import React, {Component} from 'react';
import {Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View, Dimensions} from "react-native";
import Color from "../util/Colors";
import {inject, observer} from "mobx-react";
import {computed} from "mobx";
import {NavigationActions} from 'react-navigation';
//import SFX from "../util/Sound";
import Events from "../util/Util";
import Button from "../component/Button";
import Prompt from "../component/Prompt";
import UserLogCard from "../component/UserLogCard";
//import {countDown} from "../util/Time";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ParticleView from "../util/ParticleView";

let loopSound = null;

@inject(["store"])
@observer
class DayScreen extends Component {

    called = false;

    createAcc = () => {
        //this.props.store.createAccount();
    };

    handleNavigation = (name, params) => {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: name, params})
            ]
        });
        this.props.navigation.dispatch(resetAction)
    };

    timeFormat = (date) => {
        let h = date.getHours();
        let m = date.getMinutes();
        let x = h >= 12 ? 'ከምሽቱ' : 'ከቀኑ';
        h = h % 12;
        h = h ? h : 12;
        m = m < 10 ? '0' + m : m;
        return x + " " + h + ':' + m;
    };

    constructor(props) {
        super(props);
        this.state = {
            countDownTimer: "",
            show: false
        }
    }

    static addToCalendar(game) {
        Events.addEvent({
            title: "የሜዳ ሺ ማስታወሻ",
            description: "ሜዳ ጨዋታ በተጠቀሰው ሰአት ይጀምራል",
            begin: new Date(game.start_date + "T" + game.start_time).getMilliseconds(),
            end: new Date(game.start_date + "T" + game.start_time).getMilliseconds(),
        })
    }

    componentWillReact(){
        if (this.props.store.timeLapse <= 311 && this.props.store.timeLapse !== 0) {
            this.handleNavigation('IntroScreen');
        }
    }

    render() {
        const {game, questions, startIntro, timeLapse} = this.props.store;
        if (Object.keys(questions).length !== 0 && !this.called) {
            this.called = true;
            this.handleNavigation('QuestionScreen');
            console.log("Change To Question Screen " + JSON.stringify(questions));
        }

        return (
            <ImageBackground
                source={require('../asset/game_stay_tuned.png')}
                style={styles.dayScreenWrapper}>
                <ParticleView style={{...StyleSheet.absoluteFillObject}}/>
                <TouchableOpacity style={styles.dayScreenHelpIcon} onPress={()=>{
                    this.props.navigation.navigate('FAQ')
                }}>
                    <Icon name="help" size={24}color={Color.WHITE}/>
                </TouchableOpacity>

                <Prompt
                    title="የላከዎት ሰው ስልክ ቁጥር ያስገቡ"
                    placeholder="+251 --- --- ----"
                    visible={this.state.show}
                    submitText={"ላክ"}
                    cancelText={"ይቅር"}
                    onCancel={() => this.setState({
                        show: false,
                        message: "You cancelled"
                    })}
                    onSubmit={(value) => {
                        this.setState({
                            show: false,
                        })
                        this.props.store.updateReferrerLife(value)
                    }}/>
                {/* <Animation
                    ref={animation => this.animation = animation}
                    loop={true}
                    style={styles.dayScreenLoopAnimation}
                    source="day_screen.json"
                />*/}
                <View style={styles.dayScreenNextGameWrapper}>
                    <Image
                        resizeMode='contain'
                        style={styles.dayScreenNextGameLogo}
                        source={require('../asset/logo.png')}
                    />

                    <View>
                        <Text style={styles.dayScreenNextGameText}>
                            ቀጣይ ጨዋታ
                        </Text>
                        <TouchableOpacity onPress={(e) => this.props.navigation.navigate('FAQ')}>
                            <Text style={styles.dayScreenNextGameInfo}>
                                {
                                    (typeof game[0] !== "undefined") ?
                                        `ዛሬ ${this.timeFormat(new Date(game[0].start_time))} ሰዓት ከ${game[0].prize} ብር ሽልማት ጋር`
                                        : "እባክዎ ጥቂት ይጠብቁ"
                                }
                            </Text>
                        </TouchableOpacity>
                        {/*<NoticeView/>*/}
                    </View>
                </View>
                <View>
                    <View style={styles.dayScreenBottomWrapper}>
                        <UserLogCard/>
                        <View style={styles.dayScreenButtonWrapper}>
                            <Button
                                text="አሸናፊዎች"
                                onPress={() => {
                                    this.props.navigation.navigate('LeaderBoard');
                                }}
                            />
                            <Button
                                text="ሕይወት"
                                onPress={() => {
                                    this.props.navigation.navigate('ExtraLife');
                                }}
                            />
                            <Button
                                text="የላከኝ አለ"
                                onPress={() => {
                                    this.setState({
                                        show: true,
                                    });
                                    //this.createAcc()
                                }}
                            />
                        </View>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    dayScreenWrapper: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-around',
        alignItems: "center"
    },
    dayScreenNextGameLogo: {
        alignSelf: 'center',
        width: 150,
        height: 100
    },
    dayScreenLoopAnimation: {
        ...StyleSheet.absoluteFillObject,
        alignSelf: 'center',
        width: "100%",
        marginTop: -60,
        height: "55%",
        transform: [
            {scale: 1.5}
        ]
    },
    dayScreenHelpIcon: {
        position: "absolute",
        top:0,
        right:0,
        backgroundColor: Color.TRANSPARENT_GRAY,
        paddingVertical: 3,
        paddingHorizontal: 4,
        margin: 10,
        borderRadius: 15
    },
    dayScreenNextGameWrapper: {
        color: Color.GRADIENT_START,
        paddingHorizontal: 5,
        fontSize: 25,
        borderRadius: 5
    },
    dayScreenNextGameText: {
        color: Color.GRADIENT_START,
        backgroundColor: Color.WHITE,
        paddingHorizontal: 5,
        fontSize: 25,
        width: 150,
        marginVertical: 10,
        alignSelf: "center",
        textAlign: 'center',
        borderRadius: 5
    },
    dayScreenNextGameInfo: {
        color: Color.WHITE,
        fontSize: 30,
        textAlign: 'center',
    },
    dayScreenNextGameCountDown: {
        color: Color.WHITE,
        fontSize: 50,
        textAlign: 'center',
    },
    dayScreenNextGameBelowCountDown: {
        color: Color.WHITE,
        fontSize: 20,
        textAlign: 'center',
    },
    dayScreenLifeWrapper: {
        display: "flex",
        width: 75,
        height: 75,
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    dayScreenLifeHeartIcon: {
        ...StyleSheet.absoluteFillObject,
    },
    dayScreenLifeAmount: {
        color: Color.MAIN_COLOR,
        fontSize: 20,
        fontWeight: "bold"
    },
    dayScreenBottomWrapper: {
        display: 'flex',
        width: "100%",
        justifyContent: 'space-around'
    },
    dayScreenBalanceText: {
        fontSize: 25,
        color: Color.WHITE,
    },
    dayScreenAddLifeWrapper: {
        borderRadius: 10,
        borderColor: Color.WHITE,
        borderWidth: 3,
        alignSelf: "center",
        backgroundColor: 'rgba(255, 255, 255, 0.3)',
        color: Color.WHITE,
        marginVertical: 15,
    },
    dayScreenAddLifeText: {
        marginHorizontal: 15,
        paddingVertical: 7,
        fontSize: 15,
        alignSelf: "center",
        color: Color.WHITE,
    },
    dayScreenCurrentBalance: {
        marginLeft: 5,
        marginTop: 20,
        fontSize: 30,
        color: Color.WHITE,
    },
    dayScreenButtonWrapper: {
        width: "100%",
        paddingLeft: "3%",
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-around'
    },
});

DayScreen.propTypes = {};
DayScreen.defaultProps = {};

export default DayScreen;
