import React, {Component} from 'react';
import {Dimensions, StatusBar, StyleSheet} from 'react-native';
import Main from "./main";
import {inject, observer} from 'mobx-react'
import QuestionMain from "../component/QuestionMain";
import Color from "../util/Colors";
import {NavigationActions} from "react-navigation";
import ParticleView from "../util/ParticleView";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

@inject(["store"])
@observer
class QuestionScreen extends Component {

    _navigateTo = (routeName: string) => {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: routeName}),
            ],
        });
        this.props.navigation.dispatch(resetAction);
    };

    constructor(props) {
        super(props);
        this.state = {
            answer: "",
            text: ""
        }
    }

    render() {
        if (this.props.store.gameFinished) {
            this._navigateTo("LeaderBoard")
        }
        return (
            <Main mainStyles={styles.questionScreenMainWrapper}>
                <ParticleView style={{...StyleSheet.absoluteFillObject}}/>
                <StatusBar
                    showHideTransition="fade"
                    hidden={false}/>
                <QuestionMain
                    question={this.props.store.questions}/>
            </Main>
        );
    }
}

const styles = StyleSheet.create({
    questionScreenMainWrapper: {
        flex: 1
    },
    questionScreenNewMessageWrapper: {
        width: width,
        flexDirection: 'row'
    },
    questionScreenNewMessageIcon: {
        alignSelf: 'flex-end',
        borderRadius: 50,
        padding: 5,
        backgroundColor: Color.LIGHT_GRAY
    },
    questionScreenMessageInputText: {
        width: "85%",
        fontSize: 16,
    },
    questionScreenChatView: {
        position: 'absolute',
        top: 0
    },
});

QuestionScreen.propTypes = {};
QuestionScreen.defaultProps = {};

export default QuestionScreen;
