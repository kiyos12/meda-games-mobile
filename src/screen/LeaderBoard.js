import React, {Component} from 'react';
import Main from "./main";
import {FlatList, ProgressBarAndroid, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Color from "../util/Colors";
import {inject, observer} from 'mobx-react'
import ListItem from "../component/ListItem";
import EmojiAvatar from "../component/EmojiAvatar";
import ParticleView from "../util/ParticleView";

@inject(["store"])
@observer
class LeaderBoard extends Component {

    allTimeTab = () => {
        this.setState({
            thisWeek: false,
            allTime: true
        })
    };
    thisWeekTab = () => {
        this.setState({
            thisWeek: true,
            allTime: false
        })
    };

    constructor(props) {
        super(props);
        this.state = {
            thisWeek: true,
            allTime: false,
        }
    }

    componentDidMount() {
        this.props.store.getUsers();
        /*BackHandler.addEventListener('hardwareBackPress', function () {
            AppState.addEventListener('change', (state) => {
                if (state === 'active') {
                    this.props.navigation.navigate('DayScreen')
                }
            });
            return false;
        });*/
    }


    render() {
        return (
            <Main>
                {this.props.store.users.length > 0 ?
                    <ScrollView>
                        <View style={styles.leaderBoardTabWrapper}>
                            <TouchableOpacity onPress={() => this.thisWeekTab()}>
                                <Text
                                    style={(this.state.thisWeek) ? styles.leaderBoardTabSelected : styles.leaderBoardTabUnselected}>
                                    የዚህ ሳምንት
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.allTimeTab()}>
                                <Text
                                    style={(this.state.allTime) ? styles.leaderBoardTabSelected : styles.leaderBoardTabUnselected}>
                                    የሁሉም ጊዜ
                                </Text>
                            </TouchableOpacity>


                        </View>
                        <View style={styles.leaderBoardScreenTop3Wrapper}>
                            <View style={styles.leaderBoardScreenAvatarWrapper}>
                                <Text style={styles.leaderBoardScreenTop2}>
                                    {this.props.store.users[1].avatar_emoji}
                                </Text>
                                <Text
                                    style={styles.leaderBoardScreenTop3Name}>{this.props.store.users[1].meda_name}</Text>
                                <Text
                                    style={styles.leaderBoardScreenTop3AmountWon}>{this.props.store.users[1].won + " ብር"} </Text>
                            </View>
                            <View style={styles.leaderBoardScreenAvatarWrapper}>
                                <EmojiAvatar
                                    unicode={this.props.store.users[0].avatar_emoji}
                                    style={styles.leaderBoardScreenTop1}/>
                                <Text
                                    style={styles.leaderBoardScreenTop3Name}>{this.props.store.users[0].meda_name}</Text>
                                <Text
                                    style={styles.leaderBoardScreenTop3AmountWon}>{this.props.store.users[0].won + " ብር"}</Text>
                            </View>
                            <View style={styles.leaderBoardScreenAvatarWrapper}>
                                <Text style={styles.leaderBoardScreenTop3}>
                                    {this.props.store.users[2].avatar_emoji}
                                </Text>
                                <Text
                                    style={styles.leaderBoardScreenTop3Name}>{this.props.store.users[2].meda_name}</Text>
                                <Text
                                    style={styles.leaderBoardScreenTop3AmountWon}>{this.props.store.users[2].won + " ብር"}</Text>
                            </View>

                        </View>
                        <View style={styles.leaderBoardScreenSeparator}/>
                        <FlatList
                            style={styles.leaderBoardScreenList}
                            ItemSeparatorComponent={() =>
                                <View style={styles.leaderBoardScreenSeparator}/>
                            }
                            data={this.props.store.users.splice(3)}
                            renderItem={({item, index}) => {
                                return (
                                    <ListItem
                                        item={item}
                                        index={index}
                                    />
                                )
                            }}
                        />
                    </ScrollView> :
                    <ProgressBarAndroid/>
                }

            </Main>
        );
    }
}

const styles = StyleSheet.create({
    leaderBoardHeader: {
        flexDirection: 'row',
    },
    leaderBoardScreenTitle: {
        display: "flex",
        alignSelf: "center",
        color: Color.WHITE,
        fontWeight: 'bold',
        fontSize: 25,
        marginTop: 15
    },
    leaderBoardTabWrapper: {
        display: "flex",
        justifyContent: "center",
        flexDirection: 'row',
        marginTop: 15,
        marginBottom: 15,
        width: "100%"
    },
    leaderBoardTabSelected: {
        borderRadius: 15,
        borderWidth: 1,
        borderColor: Color.WHITE,
        backgroundColor: Color.WHITE,
        color: Color.MAIN_COLOR,
        paddingHorizontal: 25,
        marginHorizontal: 5,
        paddingVertical: 5
    },
    leaderBoardTabUnselected: {
        borderRadius: 15,
        borderWidth: 1,
        borderColor: Color.WHITE,
        backgroundColor: Color.MAIN_COLOR,
        color: Color.WHITE,
        paddingHorizontal: 25,
        marginHorizontal: 5,
        paddingVertical: 5
    },
    leaderBoardScreenTop3Wrapper: {
        display: "flex",
        justifyContent: "center",
        flexDirection: 'row',
        marginTop: 20,
        width: "100%",
        marginBottom: 10
    },
    leaderBoardScreenTop1: {
        width: 100,
        height: 100,
        marginHorizontal: 15,
        borderRadius: 50
    },
    leaderBoardScreenTop2: {
        width: 75,
        height: 75,
        marginRight: 15,
        textAlign: 'center',
        fontSize: 40,
        borderRadius: 37,
        backgroundColor: Color.WHITE2,
        alignSelf: 'flex-end'
    },
    leaderBoardScreenTop3: {
        width: 75,
        height: 75,
        borderRadius: 37,
        fontSize: 40,
        textAlign: 'center',
        alignSelf: 'flex-end',
        backgroundColor: Color.WHITE2,
        marginLeft: 15,
    },
    leaderBoardScreenAvatarWrapper: {
        alignItems: "center",
        alignSelf: "flex-end"
    },
    leaderBoardScreenTop3Name: {
        color: Color.WHITE,
        fontSize: 18,
        fontWeight: "bold",
        marginVertical: 10
    },
    leaderBoardScreenTop3AmountWon: {
        color: Color.WHITE,
        fontSize: 15,
        backgroundColor: Color.MAIN_COLOR_DARK,
        borderRadius: 10,
        paddingHorizontal: 10
    },
    leaderBoardScreenSeparator: {
        marginVertical: 10,
        width: "90%",
        alignSelf: 'center',
        borderBottomColor: Color.WHITE,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    leaderBoardScreenList: {
        marginTop: 10,
    },
    leaderBoardScreenScrollWrapper: {
        flex: 1
    },

});

LeaderBoard.propTypes = {};
LeaderBoard.defaultProps = {};

export default LeaderBoard;
