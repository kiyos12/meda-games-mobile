import React, {Component} from 'react';
import Main from "./main";
import {Image, StyleSheet, Text, View} from "react-native";
import Color from "../util/Colors";
import {inject, observer} from 'mobx-react'
import ConfettiView from "../util/ConfettiView";
import AvatarView from "../component/AvatarView";
import * as Animatable from 'react-native-animatable';

@inject(['store'])
@observer
class WinnerScreen extends Component {
    render() {
        return (
            <Main mainStyles={styles.winnerScreenMainWrapper}>
                <Text style={styles.winnerScreenText}> አሸናፊ</Text>
                <Animatable.View
                    animation="bounceIn"
                    duration={2000}
                    style={styles.winnerScreenAnimatedStarWrapper}>
                    <View style={styles.winnerScreenStarWrapper}>
                        <Image
                            source={require('../asset/shooting_stars.png')}
                            style={styles.winnerScreenBigStar}
                        />
                        <Text style={styles.winnerScreenWonAmount}>50</Text>
                        <Text style={styles.winnerScreenWonCurrency}>ብር</Text>
                    </View>
                    <AvatarView avatarStyles={styles.winnerScreenUserAvatar}/>
                </Animatable.View>
                <ConfettiView style={styles.winnerScreenAnimatedKonfetti}/>

            </Main>
        );
    }
}

const styles = StyleSheet.create({
    winnerScreenMainWrapper: {
        display: "flex",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    winnerScreenAnimatedStarWrapper: {
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        width: 280,
        height: 280
    },
    winnerScreenAnimatedKonfetti: {
        ...StyleSheet.absoluteFillObject
    },
    winnerScreenStarWrapper: {
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        width: 230,
        height: 230
    },
    winnerScreenBigStar: {
        ...StyleSheet.absoluteFillObject,
        width: 250,
        height: 250
    },
    winnerScreenUserAvatar: {
        marginTop: -70
    },
    winnerScreenText: {
        color: Color.WHITE,
        marginBottom: 50,
        fontSize: 40,
        fontWeight: 'bold'
    },
    winnerScreenWonAmount: {
        color: Color.MAIN_COLOR,
        fontSize: 50,
        fontWeight: 'bold'
    },
    winnerScreenWonCurrency: {
        color: Color.LIGHT_GRAY,
        fontSize: 30,
        marginTop: -15
    }
});

WinnerScreen.propTypes = {};
WinnerScreen.defaultProps = {};

export default WinnerScreen;
