import React, {Component} from 'react';
import {StatusBar, StyleSheet, View} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Color from "../util/Colors";

class Main extends Component {
    render() {
        return (
            <LinearGradient
                style={styles.mainWrapper}
                colors={[Color.GRADIENT_START, Color.GRADIENT_END]}>
                <StatusBar
                    backgroundColor={Color.GRADIENT_START}
                    barStyle="light-content"
                />
                <View style={this.props.mainStyles}>
                    {this.props.children}
                </View>
            </LinearGradient>
        );
    }
}

const styles = StyleSheet.create({
    mainWrapper: {
        display: "flex",
        flex: 1,
    },
});

Main.propTypes = {};
Main.defaultProps = {};

export default Main;
