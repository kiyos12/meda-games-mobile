import React, {Component} from 'react';
import {Dimensions, StatusBar, StyleSheet, View} from "react-native";
import {inject, observer} from 'mobx-react'
import Video from "react-native-video";
import SFX from "../util/Sound";
import NoticeView from "../component/NoticeView";
import {NavigationActions} from 'react-navigation';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

@inject(['store'])
@observer
class IntroScreen extends Component {

    loopSound;

    handleNavigation = (name) => {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: name})
            ]
        });
        this.props.navigation.dispatch(resetAction)
    };

    constructor(props) {
        super(props);
        this.state = {
            countdown: false,
            timeLeft: 0,
            mute: true,
        }
    }

    componentDidMount() {
        let left = this.props.store.timeLapse;
        console.log("Time left " + left+" toSeek "+(305-left)+ " inMinutes "+ (305-left)/60);
        let toSeek = 305 - left;

        if(toSeek > 274){
            this.setState({
                countdown: true,
                mute: false 
            }, ()=>{
                this.introVideo.seek(toSeek-274);
            })
        }else{
            this.loopSound = SFX.play("loop.aac", false, (typeof left !== 'undefined') ? toSeek : 0, (success) => {
            this.setState({
                countdown: true,
                mute: false
            })
        });
        }        
        
    }

    componentWillUnmount() {
        SFX.stop(this.loopSound)
    }

    render() {
        const {game, questions} = this.props.store;
        if (Object.keys(questions).length !== 0 && !this.called) {
            //SFX.stop(this.loopSound);
            this.called = true;
            this.handleNavigation('QuestionScreen');
        }
        return (
            <View style={{flex: 1, justifyContent: "center"}}>
                <StatusBar
                    showHideTransition="slide"
                    hidden={true}
                />
                <NoticeView visible={!this.state.countdown}/>
                {
                    (!this.state.countdown)?
                        <Video
                            resizeMode={"cover"}
                            muted={this.state.mute}
                            repeat={true}
                            paused={false}
                            style={styles.introScreenIntroVideo}
                            source={require('../asset/ui.mov') }
                            fullscreen={true}
                        />:
                        <Video
                            resizeMode={"cover"}
                            muted={this.state.mute}
                            repeat={false}
                            paused={false}
                            ref={ref => this.introVideo = ref}
                            style={styles.introScreenIntroVideo}
                            source={require('../asset/countdown_intro.mp4')}
                            fullscreen={true}
                        />

                }
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    introScreenIntroVideo: {
        position: 'absolute',
        width: width,
        height: height,
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});

IntroScreen.propTypes = {};
IntroScreen.defaultProps = {};

export default IntroScreen;
