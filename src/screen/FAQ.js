import {Image, Linking, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import Colors from "../util/Colors";
import Accordion from "react-native-collapsible/Accordion";

const SECTIONS = [
    {
        title: 'አጨዋወት',
        content:
        '1. በመጀመሪያ, የቀጥታ ጨዋታውን በሰዓት ይቀላቀሉ: ማስታወሻ እንልክሎታለን። አርፋጅ ጨዋታውን መመልከት ብቻ ይችላል።\n\n' +
        '2. በፍጥነት ያስቡ: ጥያቄዎችን ይመልሱ ከ10 ሰከንዶች በታች በሆነ ግዜ ውስጥ ትክክለኛ መልስ ይምረጡ።\n\n' +
        '3. አንድ መልስ ከተሳሳቱ ወይም ካረፈዱ ከጨዋታው የባረራሉ: ሽልማቱን ማሸነፍም አይችሉም።\n\n' +
        '4. ሁሉንም በትክክል መልሰው ሽልማቱን ይውሰዱ፡ ሁሉንም ጥያቄዎች በትክክል ከመለሱ ተጫዋቾች ጋር ሽልማቱን ይካፈሉ።\n\n' +
        '5. ነብስ: ባንድ ጨዋታ አንድ ነብስ መጠቀም ይችላሉ!\n\n' +
        '6. ከማንኛውም የዳሽን ባንክ ቅርንጫፍ ብሮን መውሰድ ይችላሉ',
        icon: require('../asset/play.png')
    },
    {
        title: 'ተደጋግሞ የሚነሱ ጥያቄዎች',
        content: ' "አጨዋወት" የሚለውን ተመልከት',
        icon: require('../asset/faq.png')
    },
    {
        title: 'እርዳታ ያግኙ',
        content: 'Email us: help@meda.im',
        icon: require('../asset/help.png')
    },
    {
        title: 'አስተያየት ይስጡን',
        content: 'Email us: support@meda.im',
        icon: require('../asset/stars.png')
    }
];

class FAQ extends React.Component {

    static _renderHeader(section) {
        return (
            <View style={styles.faqHeaderWrapper}>
                <Image
                    style={styles.faqIcon}
                    source={section.icon}
                    resizeMode="contain"
                />
                <Text style={styles.faqHeaderText}>{section.title}</Text>
            </View>
        );
    }

    static _renderContent(section) {
        return (
            <View>
                <Text style={styles.faqContentText}>{section.content}</Text>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.faqWrapper}>
                <Text style={styles.faqHeaderLabel}>
                    ተደጋግመው የሚነሱ ጥያቄዎች
                </Text>
                <Accordion
                    sections={SECTIONS}
                    renderHeader={FAQ._renderHeader}
                    renderContent={FAQ._renderContent}
                    touchableComponent={TouchableOpacity}/>
                <View style={styles.faqFooter}>
                    <TouchableOpacity style={styles.faqSocialIconWrapper} onPress={() => {
                        Linking.openURL("https://www.facebook.com/MedaChatApp/");
                    }}>
                        <Image
                            style={styles.faqSocialIcons}
                            resizeMode="contain"
                            source={require('../asset/socicon_facebook.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.faqSocialIconWrapper} onPress={() => {
                        Linking.openURL("https://www.facebook.com/MedaChatApp/");
                    }}>
                        <Image
                            style={styles.faqSocialIcons}
                            resizeMode="contain"
                            source={require('../asset/socicon_instagram.png')}
                        />
                    </TouchableOpacity>

                </View>
            </View>


        );
    }
}

const styles = StyleSheet.create({
    faqWrapper: {
        flex: 1,
        alignItems: 'center',
    },
    faqHeaderLabel: {
        color: Colors.MAIN_COLOR,
        fontSize: 23,
        alignSelf: 'center',
        textAlign: 'center',
        marginTop: 50,
        marginBottom: 20
    },
    faqHeaderWrapper: {
        flexDirection: 'row',
        marginTop: 20,
        marginHorizontal: 35,
    },
    faqIcon: {
        width: 35,
        height: 30,
        marginVertical: 5,
        marginRight: 15
    },
    faqHeaderText: {
        fontSize: 20,
        color: Colors.BLACK,
        alignSelf: 'center',
        width: "90%"
    },
    faqContentText: {
        fontSize: 15,
        marginHorizontal: 20
    },
    faqFooter: {
        alignSelf: "flex-end",
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        margin: 20,
        flexDirection: 'row'
    },
    faqSocialIconWrapper: {
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 15,
        borderColor: Colors.LIGHT_GRAY,
        borderWidth: 1,
        width: 50,
        padding: 10,
        height: 50
    },
    faqSocialIcons: {
        width: 25,
        height: 25
    }
});

export default FAQ;