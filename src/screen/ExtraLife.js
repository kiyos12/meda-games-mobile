import React, {Component} from 'react';
import {Share, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import * as Animatable from 'react-native-animatable';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Color from "../util/Colors";
import {inject, observer} from 'mobx-react'

@inject(['store'])
@observer
class ExtraLife extends Component {
    render() {
        return (
            <View style={styles.extraLifeMainWrapper}>
                <Text style={styles.extraLifeLabel}>ተጨማሪ ህይወት</Text>
                <Animatable.View
                    animation='pulse'
                    easing="ease-out"
                    iterationCount="infinite"
                    style={styles.extraLifeWrapper}>
                    <View style={styles.extraLifeHeartAndTextWrapper}>
                        <Icon
                            style={styles.extraLifeHeartIcon}
                            name="heart"
                            size={100}
                            color={Color.MAIN_COLOR}/>
                        <Text style={styles.extraLifeAmount}>{this.props.store.user.life}</Text>
                    </View>
                </Animatable.View>

                <Text style={styles.extraLifeDescText}>
                    የተሳሳተ መልስ ከጨዋታው ተባረሩ? ተጨማሪ ሕይወት ካሎት ጨዋታውን መቀጠል ይችላሉ። ጓደኛዎን ወደ ሜዳ ጨዋታ ይጋብዙ እና ተጨማሪ ሕይወት ያግኙ:: የጋበዙት
                    ጉዋደኛ ከታች ያለውን የርሶን የግብዣ ኮድ ከተጠቀሙ ብቻ ተጨማሪ ህይወት ያገኛሉ
                </Text>

                <View style={styles.extraLifeShareMainWrapper}>
                    <Text style={styles.extraLifeYourCodeLabel}>የርሶ የግብዣ ኮድ</Text>
                    <View style={styles.extraLifeShareWrapper}>
                        <Text style={styles.extraLifeShareUsername}>{this.props.store.user.phone_number}</Text>
                        <TouchableOpacity style={styles.extraLifeShareButton} onPress={() => {
                            Share.share({
                                message: `ሜዳ ሺ ላይ የኔን የግብዣ ኮድ አስገቡ （${this.props.store.user.phone_number}） ኢትዮጵያዊነት መልካምነት`,
                                url: "https://play.google.com/store/apps/details?id=com.a360ground.meda",
                                title: "የማጋርያ ማስፈንጠርያ"
                            }).then(done => console.log("sent"))
                        }}>
                            <Text style={styles.extraLifeShareText}>አጋራ</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    extraLifeMainWrapper: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: Color.WHITE
    },
    extraLifeHeartAndTextWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        ...StyleSheet.absoluteFillObject
    },
    extraLifeHeartIcon: {
        alignSelf: 'center',
    },
    extraLifeWrapper: {
        display: "flex",
        width: 150,
        justifyContent: "center",
        alignItems: "center",
        height: 150,
        borderRadius: 75,
        backgroundColor: Color.WHITE2,
        marginTop: 10,

    },
    extraLifeAmount: {
        color: Color.WHITE,
        fontSize: 35,
        alignSelf: 'center',
        fontWeight: "bold",
        position: 'absolute',
        zIndex: 9999,
    },
    extraLifeLabel: {
        color: Color.MAIN_COLOR,
        fontSize: 30,
    },
    extraLifeShareWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 5,
        borderRadius: 27,
        paddingVertical: 5,
        width: "100%",
        borderColor: Color.WHITE2,
        borderWidth: 2,
        fontSize: 30,
    },
    extraLifeYourCodeLabel: {
        marginVertical: 10,
        fontSize: 15,
    },
    extraLifeShareUsername: {
        alignSelf: 'center',
        fontSize: 25,
        marginLeft: 15,
    },
    extraLifeShareButton: {
        borderRadius: 20,
        width: "30%",
        backgroundColor: Color.MAIN_COLOR,
    },
    extraLifeShareText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: Color.WHITE,
        marginVertical: 12,
        textAlign: 'center',
        alignSelf: 'center',
    },
    extraLifeDescText: {
        width: "90%",
        textAlign: 'center',
        fontSize: 16,
        lineHeight: 30,
        color: Color.BLACK
    },
    extraLifeShareMainWrapper: {
        textAlign: 'center',
        fontSize: 16,
        lineHeight: 30,
        alignItems: 'center',
        width: '80%',
        color: Color.BLACK
    },
    extraLifeShareMainWrapper: {
        textAlign: 'center',
        fontSize: 16,
        lineHeight: 30,
        alignItems: 'center',
        width: '80%',
        color: Color.BLACK
    },
});

ExtraLife.propTypes = {};
ExtraLife.defaultProps = {};

export default ExtraLife;
