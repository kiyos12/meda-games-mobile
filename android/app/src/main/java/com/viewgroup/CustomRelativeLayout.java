package com.viewgroup;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;

/**
 * Created by kiyos on 4/10/2018.
 */

public class CustomRelativeLayout extends ViewGroupManager<ViewGroup>{

    @Override
    public String getName () {
        return CustomRelativeLayout.class.getSimpleName ();
    }

    @Override
    protected ViewGroup createViewInstance ( ThemedReactContext reactContext ) {

        FrameLayout frameLayout = new FrameLayout (reactContext);

        frameLayout.setClipChildren (false);

        return frameLayout;
    }
}
