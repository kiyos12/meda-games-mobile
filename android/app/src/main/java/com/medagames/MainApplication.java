package com.medagames;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.BV.LinearGradient.LinearGradientPackage;
import com.airbnb.android.react.lottie.LottiePackage;
import com.brentvatne.react.ReactVideoPackage;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.instacart.library.truetime.TrueTime;
import com.medagames.autocomplete.RCAutoCompleteTextViewPackage;
import com.medagames.confetti.ConfettiViewPackage;
import com.medagames.modal.MedaModalPackage;
import com.medagames.particles.RCParticleViewPackage;
import com.medagames.util.UtlPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.viewgroup.CustomRelativeLayoutPackage;
import com.zmxv.RNSound.RNSoundPackage;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost (this) {
        @Override
        public boolean getUseDeveloperSupport () {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages () {
            return Arrays.<ReactPackage>asList (
                    new MainReactPackage (),
                    new VectorIconsPackage (),
                    new LinearGradientPackage (),
                    new RNSoundPackage (),
                    new LottiePackage (),
                    new MedaModalPackage (),
                    new ConfettiViewPackage (),
                    new UtlPackage (),
                    new ReactVideoPackage (),
                    new CustomRelativeLayoutPackage (),
                    new RCAutoCompleteTextViewPackage (),
                    new RCParticleViewPackage ()
            );
        }

        @Override
        protected String getJSMainModuleName () {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost () {
        return mReactNativeHost;
    }

    @Override
    public void onCreate () {
        super.onCreate ();
        SoLoader.init (this, /* native exopackage */ false);
        initTrueTime ();
    }

    private void initTrueTime () {
        new InitTrueTimeAsyncTask ().execute ();
    }

    // a little part of me died, having to use this
    private class InitTrueTimeAsyncTask extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground ( Void... params ) {
            try {
                TrueTime.build ()
                        //.withSharedPreferences(SampleActivity.this)
                        .withLoggingEnabled (false)
                        .withSharedPreferencesCache (MainApplication.this)
                        .withConnectionTimeout (3_1428)
                        .initialize ();
            } catch (IOException e) {
                e.printStackTrace ();
                Log.e (MainApplication.class.getSimpleName (), "something went wrong when trying to initialize TrueTime", e);
            }
            return null;
        }
    }
}
