package com.medagames.util;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.tech.Ndef;
import android.os.Parcelable;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.instacart.library.truetime.TrueTime;
import com.medagames.time.SntpClient;

import static com.airbnb.lottie.L.TAG;

/**
 * Created by kiyos on 3/21/2018.
 */

public class UtilModule extends ReactContextBaseJavaModule {

    public UtilModule ( ReactApplicationContext reactContext ) {
        super (reactContext);
    }

    @ReactMethod
    public void addEvent ( ReadableMap params ) {
        String title = params.getString ("title");
        String description = params.getString ("description");
        long beginTime = params.getInt ("begin");
        long endTime = params.getInt ("end");

        Intent intent = new Intent (Intent.ACTION_INSERT)
                .setData (CalendarContract.Events.CONTENT_URI)
                .putExtra (CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime)
                .putExtra (CalendarContract.EXTRA_EVENT_END_TIME, endTime)
                .putExtra (CalendarContract.Events.TITLE, title)
                .putExtra (CalendarContract.Events.DESCRIPTION, description)
                .putExtra (CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        getCurrentActivity ().startActivity (intent);
    }

    @ReactMethod
    public void showNotification(String message){
        getCurrentActivity ().runOnUiThread (new Runnable () {
            @Override
            public void run () {
                getCurrentActivity ().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LOW_PROFILE);
                getCurrentActivity ().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
            }
        });
    }

    //@ReactMethod
    public static void requestTime(){
        Log.d (TAG, "requestTime: "+TrueTime.now ().toString ());
    }

    @Override
    public String getName () {
        return "Util";
    }
}
