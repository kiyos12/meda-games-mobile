package com.medagames;

import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.instacart.library.truetime.TrueTime;

import java.util.Date;

import static android.content.ContentValues.TAG;

public class MainActivity extends ReactActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
    }

    @Override
    protected void onResume () {
        super.onResume ();
        getWindow ().addFlags (WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        Toast.makeText (this, "onResume: True " + (TrueTime.isInitialized () ? TrueTime.now () : " Local " + new Date ()), Toast.LENGTH_LONG).show ();
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */




    @Override
    protected String getMainComponentName () {
        return "MedaGames";
    }
}
