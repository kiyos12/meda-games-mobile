import {requireNativeComponent, ViewPropTypes} from 'react-native'
import PropTypes from 'prop-types';

const iface = {
    name: 'AutoComplete',
    propTypes: {
        data: PropTypes.arrayOf(PropTypes.string),
        ...ViewPropTypes, //include the default view properties
    },
};

export default requireNativeComponent('RCAutoCompleteTextView', iface)