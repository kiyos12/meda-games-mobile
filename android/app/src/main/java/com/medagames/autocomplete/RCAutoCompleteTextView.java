package com.medagames.autocomplete;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

/**
 * Created by kiyos on 5/15/2018.
 */

public class RCAutoCompleteTextView extends SimpleViewManager<AutoCompleteTextView> {

    private ThemedReactContext reactContext;

    @Override
    public String getName () {
        return "RCAutoCompleteTextView";
    }

    @Override
    protected AutoCompleteTextView createViewInstance ( final ThemedReactContext themedReactContext ) {
        this.reactContext = themedReactContext;
        return new AutoCompleteTextView (themedReactContext);
    }

    @ReactProp(name = "data")
    public void setAdapter ( AutoCompleteTextView autoCompleteTextView, ReadableArray array ) {
        autoCompleteTextView.setAdapter (new ArrayAdapter<Object> (autoCompleteTextView.getContext (), android.R.layout.simple_dropdown_item_1line, array.toArrayList ()));
    }

    @ReactProp(name = "id")
    public void setId ( AutoCompleteTextView autoCompleteTextView, final String id ) {
        autoCompleteTextView.setOnItemClickListener (new AdapterView.OnItemClickListener () {
            @Override
            public void onItemClick ( AdapterView<?> adapterView, View view, int i, long l ) {
                InputMethodManager imm = ( InputMethodManager ) reactContext.getSystemService (Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput (InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                WritableMap writableMap = Arguments.createMap ();
                writableMap.putString ("id", id);
                writableMap.putString ("item", adapterView.getItemAtPosition (i).toString ());

                sendEvent (reactContext, "selected", writableMap);
            }
        });
    }

    private void sendEvent ( ReactContext reactContext,
                             String eventName,
                             @Nullable WritableMap writableMap ) {
        reactContext
                .getJSModule (DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit (eventName, writableMap);
    }
}
