package com.medagames.confetti;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.medagames.R;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

/**
 * Created by kiyos on 3/20/2018.
 */

public class ConfettiView extends SimpleViewManager<KonfettiView> {

    @Override
    public String getName () {
        return "ConfettiView";
    }

    @Override
    protected KonfettiView createViewInstance ( final ThemedReactContext reactContext ) {
        final KonfettiView konfettiView = new KonfettiView (reactContext);

        for (int i = 0; i < 1; i++)
            konfettiView.build ()
                    .addColors (
                            ContextCompat.getColor (reactContext, R.color.lt_orange),
                            ContextCompat.getColor (reactContext, R.color.lt_pink),
                            ContextCompat.getColor (reactContext, R.color.lt_purple),
                            ContextCompat.getColor (reactContext, R.color.lt_yellow),
                            ContextCompat.getColor (reactContext, R.color.dk_blue))
                    .setDirection (0.0, 359.0)
                    .setSpeed (1f, 5f)
                    .setFadeOutEnabled (true)
                    .setTimeToLive (2000L)
                    .addShapes (Shape.RECT, Shape.CIRCLE)
                    .addSizes (new Size (12, 0f), new Size (16, 6f))
                    .stream (300, 5000L);
        return konfettiView;
    }

    @ReactProp(name = "start")
    public void start ( KonfettiView konfettiView, boolean start ) {
        konfettiView.build ()
                .addColors (Color.YELLOW, Color.GREEN, Color.MAGENTA)
                .setDirection (0.0, 359.0)
                .setSpeed (1f, 5f)
                .setFadeOutEnabled (true)
                .setTimeToLive (2000L)
                .addShapes (Shape.RECT, Shape.CIRCLE)
                .addSizes (new Size (12, 5f))
                .setPosition (-50f, konfettiView.getWidth () + 50f, -50f, -50f)
                .stream (300, 5000L);
    }
}
