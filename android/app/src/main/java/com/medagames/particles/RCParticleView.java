package com.medagames.particles;

import com.doctoror.particlesdrawable.ParticlesView;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;


/**
 * Created by kiyos on 6/20/2018.
 */

public class RCParticleView extends SimpleViewManager<ParticlesView> {

    @Override
    public String getName () {
        return "ParticleView";
    }

    @Override
    protected ParticlesView createViewInstance ( ThemedReactContext reactContext ) {
        ParticlesView particlesView = new ParticlesView (reactContext);
        particlesView.start ();
        return particlesView;
    }

    @ReactProp(name = "start")
    public void startAnimation ( ParticlesView particlesView, boolean start) {
        particlesView.start ();
    }
}
