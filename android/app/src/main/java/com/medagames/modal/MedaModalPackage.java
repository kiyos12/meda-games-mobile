package com.medagames.modal;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kiyos on 3/19/2018.
 */

public class MedaModalPackage implements ReactPackage {

    @Override
    public List<NativeModule> createNativeModules ( ReactApplicationContext reactContext ) {
        List<NativeModule> nativeModules = new ArrayList<> ();
        nativeModules.add (new MedaModal (reactContext));
        return nativeModules;
    }

    @Override
    public List<ViewManager> createViewManagers ( ReactApplicationContext reactContext ) {
        return Collections.emptyList ();
    }
}
