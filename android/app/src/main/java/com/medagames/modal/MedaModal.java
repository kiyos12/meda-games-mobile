package com.medagames.modal;


import android.os.Bundle;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * Created by kiyos on 3/19/2018.
 */

public class MedaModal extends ReactContextBaseJavaModule {

    private final String TYPE_LATE = "type_late";
    private final String TYPE_ELIMINATE = "type_eliminate";
    private boolean dialogOpened = false;

    private InternetDisconnected internetDisconnected;

    public MedaModal ( ReactApplicationContext reactContext ) {
        super (reactContext);
        internetDisconnected = new InternetDisconnected ();
        internetDisconnected.setCancelable (false);
    }

    @Override
    public String getName () {
        return "medamodal";
    }

    @ReactMethod
    public void showModal (int count) {

        MedaDialogFragment medaDialogFragment = new MedaDialogFragment ();

        Bundle bundle = new Bundle ();

        bundle.putInt ("count", count);

        medaDialogFragment.setArguments (bundle);

        medaDialogFragment.show (getCurrentActivity ().getFragmentManager (), MedaModal.class.getSimpleName ());
    }

    @ReactMethod
    public void showSavedModal () {

        LifeUsedModal lifeUsedModal = new LifeUsedModal ();

        lifeUsedModal.show (getCurrentActivity ().getFragmentManager (), MedaModal.class.getSimpleName ());
    }

    @ReactMethod
    public void toggleInternetDisconnectedModal ( boolean isShow ) {

        if ( isShow && !dialogOpened ) {
            dialogOpened = true;
            internetDisconnected.show (getCurrentActivity ().getFragmentManager (), MedaModal.class.getSimpleName ());
        }
        if ( !isShow && dialogOpened ) {
            dialogOpened = false;
            internetDisconnected.dismiss ();
        }
    }

    @ReactMethod
    public void showLateModal () {

        MedaDialogLateFragment medaDialogLateFragment = new MedaDialogLateFragment ();

        medaDialogLateFragment.show (getCurrentActivity ().getFragmentManager (), MedaModal.class.getSimpleName ());
    }

    @ReactMethod
    public void showEliminateModal () {

        MedaDialogEliminateFragment medaDialogEliminateFragment = new MedaDialogEliminateFragment ();

        medaDialogEliminateFragment.show (getCurrentActivity ().getFragmentManager (), MedaModal.class.getSimpleName ());
    }

    @ReactMethod
    public void showWinnerModal ( String userAvatar, String each ) {

        Bundle bundle = new Bundle ();

        bundle.putString ("Avatar", userAvatar);

        bundle.putString ("Each", each);

        MedaDialogWinnerFragment medaDialogWinnerFragment = new MedaDialogWinnerFragment ();

        medaDialogWinnerFragment.setArguments (bundle);

        medaDialogWinnerFragment.show (getCurrentActivity ().getFragmentManager (), MedaModal.class.getSimpleName ());
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants () {
        Map<String, Object> map = new HashMap<> ();
        map.put (TYPE_LATE, TYPE_LATE);
        map.put (TYPE_ELIMINATE, TYPE_ELIMINATE);
        return map;
    }
}
