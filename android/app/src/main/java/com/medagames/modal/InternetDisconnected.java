package com.medagames.modal;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medagames.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by kiyos on 3/21/2018.
 */

public class InternetDisconnected extends DialogFragment{
    @Nullable
    @Override
    public View onCreateView ( LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState ) {
        getDialog ().getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
        View v = inflater.inflate (R.layout.modal_internet_disconnected, container, false);
        return v;
    }
}
