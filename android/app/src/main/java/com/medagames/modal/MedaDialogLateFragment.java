package com.medagames.modal;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medagames.R;

/**
 * Created by kiyos on 4/2/2018.
 */

public class MedaDialogLateFragment extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView ( LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState ) {
        getDialog ().getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
        View v = inflater.inflate (R.layout.modal_meda_late, container, false);
        v.findViewById (R.id.modal_top_icon).bringToFront ();
        v.findViewById (R.id.closeModal).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick ( View view ) {
                MedaDialogLateFragment.this.dismiss ();
            }
        });
        return v;
    }
}
