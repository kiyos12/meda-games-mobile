package com.medagames.modal;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medagames.R;

/**
 * Created by kiyos on 3/19/2018.
 */

public class MedaDialogFragment extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView ( LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState ) {
        Bundle bundle = getArguments ();
        getDialog ().getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
        View v = inflater.inflate (R.layout.modal_meda, container, false);
        TextView countTextView = v.findViewById (R.id.modal_correct_answers);
        countTextView.setText (bundle.getInt ("count") + " ትክክል መልስ");
        v.findViewById (R.id.modal_top_icon).bringToFront ();
        v.findViewById (R.id.closeModal).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick ( View view ) {
                MedaDialogFragment.this.dismiss ();
            }
        });
        return v;
    }
}
