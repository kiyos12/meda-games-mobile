package com.medagames.modal;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.medagames.R;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

/**
 * Created by kiyos on 4/2/2018.
 */

public class MedaDialogWinnerFragment extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView ( LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState ) {
        getDialog ().getWindow ().setBackgroundDrawable (new ColorDrawable (Color.TRANSPARENT));
        View v = inflater.inflate (R.layout.modal_meda_winner, container, false);

        Bundle b = getArguments ();

        SimpleDraweeView draweeView = v.findViewById (R.id.modal_avatar_icon);
        Uri imageUri = Uri.parse (b.getString ("Avatar"));
        ImageRequest request = ImageRequest.fromUri (imageUri);

        KonfettiView konfettiView = v.findViewById (R.id.konfetti);

        konfettiView.build ()
                .addColors (ContextCompat.getColor (getActivity (), R.color.lt_orange),
                        ContextCompat.getColor (getActivity (), R.color.lt_pink),
                        ContextCompat.getColor (getActivity (), R.color.lt_purple),
                        ContextCompat.getColor (getActivity (), R.color.lt_yellow),
                        ContextCompat.getColor (getActivity (), R.color.dk_blue))
                .setDirection (0.0, 359.0)
                .setSpeed (1f, 5f)
                .setPosition(this.getResources().getDisplayMetrics().widthPixels/2, -359f, -359f, 0f)
                .setFadeOutEnabled (true)
                .setTimeToLive (2000L)
                .addShapes (Shape.RECT, Shape.CIRCLE)
                .addSizes (new Size (12, 0f), new Size (16, 6f))
                .stream (300, 5000L);
        
        DraweeController controller = Fresco.newDraweeControllerBuilder ()
                .setImageRequest (request)
                .setOldController (draweeView.getController ()).build ();
        draweeView.setController (controller);

        int color = getResources().getColor(R.color.lt_white);
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(color, 10.0f);
        roundingParams.setRoundAsCircle(true);
        draweeView.getHierarchy().setRoundingParams(roundingParams);

        v.findViewById (R.id.modal_avatar_icon).bringToFront ();
        v.findViewById (R.id.closeModal).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick ( View view ) {
                MedaDialogWinnerFragment.this.dismiss ();
            }
        });

        TextView wonAmount = v.findViewById (R.id.won_amount);

        wonAmount.setText (b.getString ("Each"));

        return v;
    }
}
