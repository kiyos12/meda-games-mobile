package com.medagames.headless;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.jstasks.HeadlessJsTaskConfig;

import javax.annotation.Nullable;

/**
 * Created by kiyos on 3/23/2018.
 */

public class GameListener extends HeadlessJsTaskService {

    @Nullable
    @Override
    protected HeadlessJsTaskConfig getTaskConfig ( Intent intent ) {
        Bundle extras = intent.getExtras ();
        Log.d (GameListener.class.getSimpleName (), "getTaskConfig: ");
        if ( extras != null ) {
            return new HeadlessJsTaskConfig (
                    "GameService",
                    Arguments.fromBundle (extras),
                    20,
                    true
            );
        }
        return super.getTaskConfig (intent);
    }
}
